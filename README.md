# SkinNet

#### UnB Computer Vision  - Skin Segmentation Project

![teaser](results/teaser.png)

This repo contains training and testing code for our Skin Segmentation Project


## Contents
0. [Organization](#organization)
1. [Datasets](#datasets)
2. [Training over Source Datasets](#training-over-source-datasets)
3. [Evaluating](#evaluating)
4. [Generating Pseudo-Labels](#generating-pseudo-labels)
5. [Overleaf](#overleaf)



## Organization 
The project folder is organized as follows:
``` shell
    SkinNet
         |-- references
         |-- results
         |-- weights

``` 
The data folder should be organized as follows:

``` shell
    SkinNet
         |-- images
                    |-- compaq
                            |-- masks
                            |-- skin-images
                    |-- pratheepan
                            |-- Ground_Truth
                            |-- Pratheepan_Dataset
                    |-- sfa
                            |-- GT
                            |-- ORI
                    |-- vdm
                         |-test   
                         |-train
         |-- pseudo
                    |-- compaq
                            |-- masks
                    |-- pratheepan
                            |-- Ground_Truth
                    |-- sfa
                            |-- GT
                    |-- vdm
                         |-test   
                         |-train

``` 


## Datasets

Compaq - Dataset available upon request to the authors

SFA - http://www.sel.eesc.usp.br/sfa/

Pratheepan - http://web.fsktm.um.edu.my/~cschan/downloads_skin_dataset.html

VPU - http://www-vpu.eps.uam.es/publications/SkinDetDM

To get the datasets ready for use:
``` shell
  tar -zxvf images.tar.gz 
``` 
To correct VPU annotations:
``` shell
  python convert_vpu.py 
``` 
To correct iCCP warning on Pratheepan Labels:
``` shell
  cd (iamge base dir)/pratheepan/Ground_Truth/GroundT_FacePhoto
  mogrify *.png
  cd (iamge base dir)/pratheepan/Ground_Truth/GroundT_FamilyPhoto
  mogrify *.png
``` 

Compaq dataset has some images with no corresponding gt masks. We are discarding them.

<span style="color:red">
*ADVISE*: Compaq dataset has images inappropriate for children under 18. Precaution is required while opening the images
or the results reported over the dataset.  
</span>

## Training over Source Datasets
 (no pseudo labels)

### help

``` shell
  python train.py --help
``` 

### Same domain training commands

``` shell
  python train.py unet sfa --epochs=100 --patience=10 --lr=0.0002 --decay=0.005
  python train.py unet compaq --epochs=100 --patience=10 --lr=0.0002 --decay=0.005
  python train.py unet pratheepan --epochs=100 --patience=10 --lr=0.0002 --decay=0.005
  python train.py unet vpu --epochs=100 --patience=10 --lr=0.0002 --decay=0.0050
  
  python train.py patch sfa --epochs=100 --patience=10 --lr=0.0001 --decay=0.002 --batch_size=1024
  python train.py patch pratheepan --epochs=200 --patience=50 --lr=0.00009 --decay=0.002 --batch_size=512
  python train.py patch vpu --epochs=200 --patience=50 --lr=0.00009 --decay=0.002 --batch_size=512
  python train.py patch compaq --epochs=200 --patience=50 --lr=0.00009 --decay=0.002 --batch_size=512
```


## Evaluating

### help

``` shell
  python evaluate.py --help
``` 


### Same domain evaluating commands


``` shell
  python evaluate.py unet unet_sfa_LR0.0001_DC0.002_10 sfa --test_proportion=.15
  python evaluate.py unet unet_compaq_LR0.0001_DC0.002_11 compaq --test_proportion=.15
  python evaluate.py unet unet_pratheepan_LR0.0001_DC0.002_12 pratheepan --test_proportion=.15
  python evaluate.py unet unet_vpu_LR0.0002_DC0.005_20 vpu --test_proportion=.15

  python evaluate.py patch patch_sfa_LR0.0001_DC0.002_27 sfa --test_proportion=.15
  python evaluate.py patch patch_pratheepan_LR0.0001_DC0.002_27 pratheepan --test_proportion=.15
  python evaluate.py patch patch_vpu_LR9e-05_DC0.002_35 vpu --test_proportion=.15
  python evaluate.py patch patch_compaq_LR9e-05_DC0.002_42 compaq --test_proportion=.15
``` 

### Cross-domain evaluating commands


``` shell
  python evaluate.py unet unet_sfa_LR0.0001_DC0.002_10 compaq  --test_proportion=1
  python evaluate.py unet unet_sfa_LR0.0001_DC0.002_10 pratheepan  --test_proportion=1
  python evaluate.py unet unet_sfa_LR0.0001_DC0.002_10 vpu  --test_proportion=1

  python evaluate.py unet unet_compaq_LR0.0001_DC0.002_11 sfa  --test_proportion=1
  python evaluate.py unet unet_compaq_LR0.0001_DC0.002_11 pratheepan  --test_proportion=1
  python evaluate.py unet unet_compaq_LR0.0001_DC0.002_11 vpu  --test_proportion=1

  python evaluate.py unet unet_pratheepan_LR0.0001_DC0.002_12 sfa  --test_proportion=1  
  python evaluate.py unet unet_pratheepan_LR0.0001_DC0.002_12 compaq  --test_proportion=1
  python evaluate.py unet unet_pratheepan_LR0.0001_DC0.002_12 vpu  --test_proportion=1

  python evaluate.py patch patch_sfa_LR0.0001_DC0.002_27 compaq  --test_proportion=1

  python evaluate.py patch patch_compaq_LR9e-05_DC0.002_42 sfa  --test_proportion=1


``` 
## Generating Pseudo-Labels

### help

``` shell
  python pseudo_labels.py --help
``` 
### Examples


``` shell
  python pseudo_labels.py unet unet_compaq_LR0.0001_DC0.002_11 sfa
  python pseudo_labels.py unet unet_compaq_LR0.0001_DC0.002_11 pratheepan
  python pseudo_labels.py unet unet_compaq_LR0.0001_DC0.002_11 vpu

  python pseudo_labels.py unet unet_pratheepan_LR0.0001_DC0.002_12 sfa
  python pseudo_labels.py unet unet_pratheepan_LR0.0001_DC0.002_12 compaq
  python pseudo_labels.py unet unet_pratheepan_LR0.0001_DC0.002_12 vpu
``` 

## Final Training and evaluation

### Supervised (Target only)

See train_all_supervised.sh and evaluate_all_supervised.sh

*EXAMPLE*<BR>
Target only <BR>
Target = SFA <BR>
Proportion of real labels =  10%, 30%, 50% 


TRAINING:
``` shell
   python train.py unet sfa --epochs=100 --patience=10 --lr=0.0002 --decay=0.005 --test_proportion=.9 --valid_proportion=.02
   python train.py unet sfa --epochs=100 --patience=10 --lr=0.0002 --decay=0.005 --test_proportion=.7 --valid_proportion=.06
   python train.py unet sfa --epochs=100 --patience=10 --lr=0.0002 --decay=0.005 --test_proportion=.5 --valid_proportion=.1
``` 


EVALUATING:
``` shell
  python evaluate.py unet unet_sfa_LR0.0002_DC0.005_45 sfa  --test_proportion=.9
  python evaluate.py unet unet_sfa_LR0.0002_DC0.005_46 sfa  --test_proportion=.7
  python evaluate.py unet unet_sfa_LR0.0002_DC0.005_47 sfa  --test_proportion=.5
``` 

### Fine Tuning

Fine Tune Only <BR>
Target = SFA<BR>
Source Model =  unet_compaq_LR0.0001_DC0.002_11<BR>
Proportion of real labels =  10%, 30%, 50% 

Remember to reduce LR<BR>


TRAINING:
``` shell
  python train.py unet sfa --weights=unet_compaq_LR0.0001_DC0.002_11 --epochs=100 --patience=10 --lr=0.00002 --decay=0.005 --test_proportion=.9  --valid_proportion=.02
  python train.py unet sfa --weights=unet_compaq_LR0.0001_DC0.002_11 --epochs=100 --patience=10 --lr=0.00002 --decay=0.005 --test_proportion=.7  --valid_proportion=.06
  python train.py unet sfa --weights=unet_compaq_LR0.0001_DC0.002_11 --epochs=100 --patience=10 --lr=0.00002 --decay=0.005 --test_proportion=.5  --valid_proportion=.1
``` 
EVALUATING:
``` shell
  python evaluate.py unet unet_sfa_LR0.0002_DC0.005_57 sfa  --test_proportion=.9
  python evaluate.py unet unet_sfa_LR0.0002_DC0.005_57 sfa  --test_proportion=.7
  python evaluate.py unet unet_sfa_LR0.0002_DC0.005_57 sfa  --test_proportion=.5
``` 



### Training with pseudo labels

Pseudo Labels Only <BR>
Target = SFA<BR>
Source of labels =  unet_compaq_LR0.0001_DC0.002_11<BR>
Proportion of real labels =  0%, 10%, 30%, 50% (valid proportion wil be calculated by the script)<BR>

TRAINING:
``` shell
   python train_pseudo.py unet sfa unet_compaq_LR0.0001_DC0.002_11 --epochs=100 --patience=10 --lr=0.0002 --decay=0.005 --test_proportion=1 
   python train_pseudo.py unet sfa unet_compaq_LR0.0001_DC0.002_11 --epochs=100 --patience=10 --lr=0.0002 --decay=0.005 --test_proportion=.9 
   python train_pseudo.py unet sfa unet_compaq_LR0.0001_DC0.002_11 --epochs=100 --patience=10 --lr=0.0002 --decay=0.005 --test_proportion=.7 
   python train_pseudo.py unet sfa unet_compaq_LR0.0001_DC0.002_11 --epochs=100 --patience=10 --lr=0.0002 --decay=0.005 --test_proportion=.5 
``` 
EVALUATING:
``` shell
  python evaluate.py unet unet_sfa_LR0.0002_DC0.005_57 sfa  --test_proportion=1
  python evaluate.py unet unet_sfa_LR0.0002_DC0.005_58 sfa  --test_proportion=.9
  python evaluate.py unet unet_sfa_LR0.0002_DC0.005_59 sfa  --test_proportion=.7
  python evaluate.py unet unet_sfa_LR0.0002_DC0.005_60 sfa  --test_proportion=.5
``` 

Combined Approach <BR>
Target = SFA<BR>
Source of labels =  unet_compaq_LR0.0001_DC0.002_11<BR>
source Model =  unet_compaq_LR0.0001_DC0.002_11<BR>
Proportion of real labels =  0%, 10%, 30%, 50% (valid proportion wil be calculated by the script)<BR>

Remember to reduce LR<BR>


TRAINING:
``` shell
   python train_pseudo.py unet sfa unet_compaq_LR0.0001_DC0.002_11 --epochs=100 --patience=10 --lr=0.0002 --decay=0.005 --test_proportion=1 
   python train_pseudo.py unet sfa unet_compaq_LR0.0001_DC0.002_11 --epochs=100 --patience=10 --lr=0.0002 --decay=0.005 --test_proportion=.9 
   python train_pseudo.py unet sfa unet_compaq_LR0.0001_DC0.002_11 --epochs=100 --patience=10 --lr=0.0002 --decay=0.005 --test_proportion=.7 
   python train_pseudo.py unet sfa unet_compaq_LR0.0001_DC0.002_11 --epochs=100 --patience=10 --lr=0.0002 --decay=0.005 --test_proportion=.5 
``` 
EVALUATING:
``` shell
  python evaluate.py unet unet_sfa_LR0.0002_DC0.005_?? sfa  --test_proportion=1
  python evaluate.py unet unet_sfa_LR0.0002_DC0.005_?? sfa  --test_proportion=.9
  python evaluate.py unet unet_sfa_LR0.0002_DC0.005_?? sfa  --test_proportion=.7
  python evaluate.py unet unet_sfa_LR0.0002_DC0.005_?? sfa  --test_proportion=.5
``` 





## Overleaf

https://www.overleaf.com/18824204wtydtyqyckcb#/70740520/

