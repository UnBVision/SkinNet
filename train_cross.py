import os
os.environ["KERAS_BACKEND"]="tensorflow"
#os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"   # see issue #152
#os.environ["CUDA_VISIBLE_DEVICES"]="1"
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
from keras.optimizers import RMSprop
from dnn_utils import segmentation_generator, patch_generator, evaluate_patch_generator
from dnn_utils import skin_preprocess, mask_preprocess, dice_loss
from dnn_utils import get_images, iou, precision, recall, get_run, get_callbacks,split
from dnn_utils import get_images_pseudo, split_pseudo, split_cross
from math import ceil

import argparse


##################################
BATCH_SIZE = 2
LR=0.0002
DECAY=0.002

IMAGE_DIR = "/d02/data/skin/images"
PSEUDO_DIR = "/d02/data/skin/pseudo"
HDF5_DIR = "weights"
DATASET = "compaq" #compaq or sfa
TEST_PROPORTION  = 0.15
VALID_PROPORTION = 0.20
TRAIN_PROPORTION = 1
EPOCHS = 100
PATIENCE = 10
ARCHITECTURE = 'unet'
WEIGHTS = 'none'
PSEUDO_MODEL = None

def train():
    imgs, gts = get_images(DATASET, IMAGE_DIR)

    train_img, train_gt, valid_img, valid_gt, test_img, test_gt = split_cross(imgs, gts,
                                                              test_p  = TEST_PROPORTION,
                                                              valid_p = VALID_PROPORTION,
                                                              train_p = TRAIN_PROPORTION)

    train_set = train_img + valid_img

    if PSEUDO_MODEL is not None:
        model_name = ARCHITECTURE + "_" + DATASET + '_P_LR' + str(LR) + '_DC' + str(DECAY)

        pseudo_imgs, pseudo_gts = get_images_pseudo(DATASET, IMAGE_DIR, PSEUDO_DIR, PSEUDO_MODEL, train_set)


        train_pseudo_img, train_pseudo_gt, valid_pseudo_img, valid_pseudo_gt = split_pseudo(pseudo_imgs, pseudo_gts, valid_p=VALID_PROPORTION)
        train_img = train_img + train_pseudo_img
        train_gt = train_gt + train_pseudo_gt
        valid_img = valid_img + valid_pseudo_img
        valid_gt = valid_gt + valid_pseudo_gt

    else:
        model_name = ARCHITECTURE + "_" + DATASET + '_LR' + str(LR) + '_DC' + str(DECAY)


    if ARCHITECTURE == 'unet':
        TEST_BATCH_SIZE = 15
        from unet_512_768 import get_unet_512
        train_datagen = segmentation_generator(train_img, mask_files=train_gt, img_preproc_func=skin_preprocess,
                                     mask_preproc_func=mask_preprocess, batch_size=BATCH_SIZE, augmentation=True,shuff=True)
        print("len_valid", len(valid_img))
        valid_datagen = segmentation_generator(valid_img, mask_files=valid_gt, img_preproc_func=skin_preprocess,
                                     mask_preproc_func=mask_preprocess, batch_size=BATCH_SIZE)
        test_datagen = segmentation_generator(test_img, mask_files=test_gt, img_preproc_func=skin_preprocess,
                                              mask_preproc_func=mask_preprocess, batch_size=TEST_BATCH_SIZE)
        train_steps = ceil(len(train_img) / BATCH_SIZE)
        valid_steps = ceil(len(valid_img) / BATCH_SIZE)
        print("steps", train_steps, valid_steps)
        model = get_unet_512()

    elif ARCHITECTURE == 'patch':
        TEST_BATCH_SIZE = 1
        n_samples = 512
        from patch_net import get_patch_net2
        train_datagen = patch_generator(train_img, mask_files=train_gt, img_preproc_func=skin_preprocess,
                                        mask_preproc_func=mask_preprocess, batch_size=BATCH_SIZE, augmentation=True,n_samples=n_samples)
        valid_datagen = patch_generator(valid_img, mask_files=valid_gt, img_preproc_func=skin_preprocess,
                                        mask_preproc_func=mask_preprocess, batch_size=BATCH_SIZE,n_samples=n_samples)
        test_datagen = evaluate_patch_generator(test_img, mask_files=test_gt, img_preproc_func=skin_preprocess,
                                                mask_preproc_func=mask_preprocess)
        train_steps = ceil(len(train_img)*n_samples/ BATCH_SIZE)
        valid_steps = ceil(len(valid_img)*n_samples / BATCH_SIZE)
        model = get_patch_net2()
    else:

        print("Invalid archtecture: ",ARCHITECTURE)


    model.compile(optimizer=RMSprop(lr=LR, decay=DECAY), loss=dice_loss, metrics=[iou, precision, recall, 'accuracy'])



    if WEIGHTS != 'none':
        model_name += '_FT'
        model.load_weights(os.path.join(HDF5_DIR, WEIGHTS+".hdf5"))

    run = get_run()
    model_name_run = model_name + "_" + str(run)

    print("Training Model:", model_name_run)



    model.fit_generator(
            train_datagen,
            steps_per_epoch=train_steps,
            epochs=EPOCHS,
            validation_data=valid_datagen,
            validation_steps=valid_steps,
            callbacks=get_callbacks(model_name_run, log_dir="log", patience=PATIENCE, hdf5_dir=HDF5_DIR),
            workers=8 )

    if ARCHITECTURE == 'unet':

        print("Evaluating:", model_name_run, len(test_img), ceil(len(test_img)/TEST_BATCH_SIZE))

        results = model.evaluate_generator(test_datagen, steps=ceil(len(test_img)/TEST_BATCH_SIZE) , workers=8)

        res_df = pd.DataFrame({'source': [DATASET],
                               'model': [model_name_run],
                               'architecure': [ARCHITECTURE],
                               'lr': [LR],
                               'decay': [DECAY],
                               'weights': [WEIGHTS]
                               }
                              )

        for metric, val in zip(model.metrics_names, results):
            res_df[metric] = round(val,4)
            print(metric+": "+"%.4f" % val)

        if os.path.exists("results/training_results.csv"):
            res_df= pd.read_csv("results/training_results_results.csv").append(res_df)

        res_df.to_csv("results/training_results_results.csv", index=False)




# Main Function
def Run():

    global DATASET, BATCH_SIZE, LR, DECAY, IMAGE_DIR, HDF5_DIR,  \
           TEST_PROPORTION, VALID_PROPORTION, TRAIN_PROPORTION,\
           PATIENCE, EPOCHS, ARCHITECTURE, WEIGHTS, PSEUDO_MODEL

    parser = argparse.ArgumentParser(description='Train model on dataset (no pseudo labels).')
    parser.add_argument("architecture", help="Architecture of the model.", type=str)
    parser.add_argument("dataset", help="Dataset", type=str, default=DATASET)
    parser.add_argument("--pseudo_model", help="Model that generated the pseudo labels", type=str, default=PSEUDO_MODEL,required=False)
    parser.add_argument("--epochs", help="Number of epochs.", type=float, default=EPOCHS, required=False)
    parser.add_argument("--weights", help="Model weights to load before training.", type=str, default=WEIGHTS, required=False)
    parser.add_argument("--patience", help="Patience.", type=float, default=PATIENCE, required=False)
    parser.add_argument("--batch_size", help="Batch size.", type=int, default=BATCH_SIZE, required=False)
    parser.add_argument("--lr", help="Learning rate.", type=float, default=LR, required=False)
    parser.add_argument("--decay", help="Learning rate decay.", type=float, default=DECAY, required=False)
    parser.add_argument("--image_dir", help="Image directory", type=str, default=IMAGE_DIR, required=False)
    parser.add_argument("--hdf5_dir", help="HDF5 Directort", type=str, default=HDF5_DIR, required=False)
    parser.add_argument("--test_proportion", help="Test Proportion.", type=float, default=TEST_PROPORTION, required=False)
    parser.add_argument("--valid_proportion", help="Valid Proportion.", type=float, default=VALID_PROPORTION, required=False)
    parser.add_argument("--train_proportion", help="Train Proportion.", type=float, default=TRAIN_PROPORTION, required=False)

    args = parser.parse_args()

    ARCHITECTURE = args.architecture
    DATASET = args.dataset
    WEIGHTS = args.weights
    PSEUDO_MODEL = args.pseudo_model
    BATCH_SIZE = args.batch_size
    EPOCHS = args.epochs
    PATIENCE = args.patience
    LR = args.lr
    DECAY = args.decay
    IMAGE_DIR = args.image_dir
    HDF5_DIR = args.hdf5_dir
    TEST_PROPORTION = args.test_proportion
    VALID_PROPORTION = args.valid_proportion
    TRAIN_PROPORTION = args.train_proportion

    train()


if __name__ == '__main__':
  Run()
