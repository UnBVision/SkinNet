#python train.py unet sfa --epochs=100 --patience=10 --lr=0.0002 --decay=0.005 --test_proportion=.9 --valid_proportion=.02
#python train.py unet sfa --epochs=100 --patience=10 --lr=0.0002 --decay=0.005 --test_proportion=.7 --valid_proportion=.06
#python train.py unet sfa --epochs=100 --patience=10 --lr=0.0002 --decay=0.005 --test_proportion=.5 --valid_proportion=.1

python train.py unet compaq --epochs=100 --patience=10 --lr=0.0002 --decay=0.005 --test_proportion=.9 --valid_proportion=.02
python train.py unet compaq --epochs=100 --patience=10 --lr=0.0002 --decay=0.005 --test_proportion=.7 --valid_proportion=.06
python train.py unet compaq --epochs=100 --patience=10 --lr=0.0002 --decay=0.005 --test_proportion=.5 --valid_proportion=.1

python train.py unet pratheepan --epochs=100 --patience=10 --lr=0.0002 --decay=0.005 --test_proportion=.9 --valid_proportion=.02
python train.py unet pratheepan --epochs=100 --patience=10 --lr=0.0002 --decay=0.005 --test_proportion=.7 --valid_proportion=.06
python train.py unet pratheepan --epochs=100 --patience=10 --lr=0.0002 --decay=0.005 --test_proportion=.5 --valid_proportion=.1

python train.py unet vpu --epochs=100 --patience=10 --lr=0.0002 --decay=0.005 --test_proportion=.9 --valid_proportion=.02
python train.py unet vpu --epochs=100 --patience=10 --lr=0.0002 --decay=0.005 --test_proportion=.7 --valid_proportion=.06
python train.py unet vpu --epochs=100 --patience=10 --lr=0.0002 --decay=0.005 --test_proportion=.5 --valid_proportion=.1
