import threading
import cv2
import numpy as np
from sklearn.utils import shuffle
import pandas as pd
import os
from keras.callbacks import Callback, EarlyStopping, ModelCheckpoint, ReduceLROnPlateau
import tensorflow as t
import glob
from sklearn.model_selection import train_test_split

THRESHOLD = 1


#################### Now make the data generator threadsafe ####################


def randomHueSaturationValue(image, hue_shift_limit=(-100, 100),
                             sat_shift_limit=(-100, 100),
                             val_shift_limit=(-100, 100), u=0.8):
    if np.random.random() < u:
        image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
        h, s, v = cv2.split(image)
        hue_shift = np.random.uniform(hue_shift_limit[0], hue_shift_limit[1])
        h = cv2.add(h, hue_shift)
        sat_shift = np.random.uniform(sat_shift_limit[0], sat_shift_limit[1])
        s = cv2.add(s, sat_shift)
        val_shift = np.random.uniform(val_shift_limit[0], val_shift_limit[1])
        v = cv2.add(v, val_shift)
        image = cv2.merge((h, s, v))
        image = cv2.cvtColor(image, cv2.COLOR_HSV2BGR)

    return image


def randomShift(image, mask,
                           shift_limit=(-0.09, 0.09),
                           u=0.8):
    if np.random.random() < u:
        height, width, channel = image.shape

        dx = round(np.random.uniform(shift_limit[0], shift_limit[1]) * width)
        dy = round(np.random.uniform(shift_limit[0], shift_limit[1]) * height)

        M = np.float32([[1, 0, dx], [0, 1, dy]])
        image = cv2.warpAffine(image, M, (width, height))
        mask = cv2.warpAffine(mask, M, (width, height))

    return image, mask


def randomHorizontalFlip(image, mask, u=0.5):
    if np.random.random() < u:
        image = cv2.flip(image, 1)
        mask = cv2.flip(mask, 1)

    return image, mask

def randomVerticalFlip(image, mask, u=0.5):
    if np.random.random() < u:
        image = cv2.flip(image, 0)
        mask = cv2.flip(mask, 0)

    return image, mask




def skin_preprocess(img):
    #im = np.empty(img.shape)
    im = img.astype(float)/255
    #X = img[:,:,0]
    #Y = img[:,:,1]
    #Z = img[:,:,2]
    #im[:,:,0] = (X.astype(float) / (X.astype(float) + Y.astype(float) + Z.astype(float) + 10)) * 3
    #im[:,:,1] = (Y.astype(float) / (X.astype(float) + Y.astype(float) + Z.astype(float) + 10)) * 3
    #im[:,:,2] = (Z.astype(float) / (X.astype(float) + Y.astype(float) + Z.astype(float) + 10)) * 3
    return im

def mask_preprocess(img):
    im = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    im[im <  THRESHOLD] = 0
    im[im >=THRESHOLD ] = 1
    #im = im.reshape((im.shape[0],im.shape[1],1))
    #im = np.zeros((img.shape[0],img.shape[1],1),np.uint8)
    #im[(img[:,:,0] + img[:,:,1] + img[:,:,2]) >THRESHOLD] = 1
    return im.reshape((im.shape[0], im.shape[1],1)).astype(float)


class threadsafe_generator(object):
    """Takes an iterator/generator and makes it thread-safe by
    serializing call to the `next` method of given iterator/generator.
    """
    def __init__(self, gen):
        self.gen = gen
        self.lock = threading.Lock()

    def __iter__(self):
        return self

    def __next__(self):
        with self.lock:
            return self.gen.__next__()


def threadsafe(f):
    """A decorator that takes a generator function and makes it thread-safe.
    """
    def g(*a, **kw):
        return threadsafe_generator(f(*a, **kw))
    return g

@threadsafe
def segmentation_generator(img_files, mask_files=None,
                           img_preproc_func=skin_preprocess, mask_preproc_func=mask_preprocess, batch_size=4,
                           shuff=False, size=768, augmentation=False):  # write the definition of your data generator

    x_batch = np.zeros((batch_size, size, size, 3))
    y_batch = np.zeros((batch_size, size, size, 1))
    batch_count = 0
    while True:
        if shuff:
            img_files_s, mask_files_s = shuffle(img_files, mask_files)
        else:
            img_files_s, mask_files_s = img_files, mask_files

        for count, (img_file, mask_file) in enumerate(zip(img_files_s, mask_files_s)):
            img = cv2.imread(img_file)
            mask = cv2.imread(mask_file)

            try:
                if img.shape != mask.shape:
                    continue
            except AttributeError:
                print("AttributeError",img_file,mask_file)
                continue

            img_bg = np.zeros((size,size,3), np.uint8)
            mask_bg = np.zeros((size,size,3), np.uint8)

            if img.shape[0] > size or img.shape[1] > size:
                max_dim = max(img.shape[0],img.shape[1])
                dest_h = int(img.shape[0] * size /max_dim)
                dest_w = int(img.shape[1] * size /max_dim)
                img = cv2.resize(img,(dest_w,dest_h))
                mask = cv2.resize(mask,(dest_w,dest_h))

            (h, w) = img.shape[:2]

            img_bg[(size-h)//2 : (size-h)//2 + h, (size-w)//2 : (size-w)//2 + w] = img
            mask_bg[(size-h)//2 : (size-h)//2 + h, (size-w)//2 : (size-w)//2 + w] = mask

            if augmentation:
                img_bg = randomHueSaturationValue(img_bg,
                                               hue_shift_limit=(-8, 8),
                                               sat_shift_limit=(-8, 8),
                                               val_shift_limit=(-20, 20))
                img_bg, mask_bg = randomShift(img_bg, mask_bg, shift_limit=(-0.1, 0.1))
                img_bg, mask_bg = randomHorizontalFlip(img_bg, mask_bg)
                img_bg, mask_bg = randomVerticalFlip(img_bg, mask_bg)

            img_bg = img_preproc_func(img_bg)
            mask_bg = mask_preproc_func(mask_bg)

            x_batch[batch_count] = img_bg
            y_batch[batch_count] = mask_bg
            batch_count += 1
            if batch_count == batch_size:
                yield x_batch, y_batch
                x_batch = np.zeros((batch_size, size, size, 3))
                y_batch = np.zeros((batch_size, size, size, 1))
                batch_count = 0
        if batch_count > 0:
            yield x_batch[:batch_count], y_batch[:batch_count]
            x_batch = np.zeros((batch_size, size, size, 3))
            y_batch = np.zeros((batch_size, size, size, 1))
            batch_count = 0

@threadsafe
def patch_generator(img_files, mask_files=None,
                           img_preproc_func=skin_preprocess, mask_preproc_func=None, batch_size=64, n_samples=512,
                           shuff=False, shape=(35, 35), augmentation=False):  # write the definition of your data generator

    x_batch = np.zeros((batch_size, shape[0], shape[1], 3))
    y_batch = np.zeros((batch_size))
    batch_count = 0

    while True:
        if shuff:
            img_files_s, mask_files_s = shuffle(img_files, mask_files)
        else:
            img_files_s, mask_files_s = img_files, mask_files


        for count, (img_file, mask_file) in enumerate(zip(img_files_s, mask_files_s)):
            img = cv2.imread(img_file)
            mask = cv2.imread(mask_file)

            try:
                if img.shape != mask.shape:
                    continue
            except AttributeError:
                print("AttributeError",img_file,mask_file)
                continue

            if img.shape[0] < 50 or img.shape[1]<50:
                continue

            if augmentation:
                img = randomHueSaturationValue(img,
                                               hue_shift_limit=(-8, 8),
                                               sat_shift_limit=(-8, 8),
                                               val_shift_limit=(-20, 20))
            img = img_preproc_func(img)
            mask = mask_preproc_func(mask)


            for i in range(n_samples):
                x_start = round(np.random.uniform(0, img.shape[1]- shape[1]))
                x_end = x_start+shape[1]
                y_start = round(np.random.uniform(0, img.shape[0]- shape[0]))
                y_end = y_start+shape[0]

                x_batch[batch_count] = img[y_start:y_end, x_start:x_end]
                y_batch[batch_count] = mask[y_start+shape[0]//2, x_start+shape[1]//2 ]

                batch_count += 1

                if batch_count == batch_size:

                    yield x_batch, y_batch
                    x_batch = np.zeros((batch_size, shape[0], shape[1], 3))
                    y_batch = np.zeros((batch_size))
                    batch_count = 0

        if batch_count >0:

            yield x_batch[:batch_count], y_batch[:batch_count]
            x_batch = np.zeros((batch_size, shape[0], shape[1], 3))
            y_batch = np.zeros((batch_size))
            batch_count = 0



@threadsafe
def evaluate_patch_generator(img_files, mask_files=None,
                           img_preproc_func=skin_preprocess, mask_preproc_func=None,
                           shape=(35, 35), augmentation=False):  # write the definition of your data generator

    batch_count = 0
    while True:
        for count, (img_file, mask_file) in enumerate(zip(img_files, mask_files)):
            img = cv2.imread(img_file)
            mask = cv2.imread(mask_file)

            try:
                if img.shape != mask.shape:
                    continue
            except AttributeError:
                print("AttributeError",img_file,mask_file)
                continue

            if img.shape[0] < 50 or img.shape[1]<50:
                continue

            img = img_preproc_func(img)
            mask = mask_preproc_func(mask)

            x_batch = np.zeros((img.shape[0]*img.shape[1], shape[0], shape[1], 3))
            y_batch = np.zeros((img.shape[0],img.shape[1],1))

            for y in np.array(range(img.shape[0]-shape[0]))+shape[0]//2:
                for x in np.array(range(img.shape[1] - shape[1])) + shape[1] // 2:
                    x_start = x - shape[0]//2
                    x_end = 1 + x + shape[0]//2
                    y_start = y - shape[1] // 2
                    y_end = 1 + y + shape[1] // 2

                    x_batch[y*img.shape[1]+x] = img[y_start:y_end, x_start:x_end]
                    y_batch[y,x] = mask[y,x]

            yield x_batch, y_batch


@threadsafe
def evaluation_generator(img_files, mask_files=None,
                           img_preproc_func=skin_preprocess, mask_preproc_func=None, size=768):  # write the definition of your data generator

    x_batch = np.zeros((1, size, size, 3))
    while True:

        for count, (img_file, mask_file) in enumerate(zip(img_files, mask_files)):
            img = cv2.imread(img_file)
            mask = cv2.imread(mask_file)

            try:
                if img.shape != mask.shape:
                    continue
            except AttributeError:
                print("AttributeError",img_file,mask_file)
                continue

            img_bg = np.zeros((size,size,3), np.uint8)

            if img.shape[0] > size or img.shape[1] > size:
                print(img_file, img.shape )
                max_dim = max(img.shape[0],img.shape[1])
                dest_h = int(img.shape[0] * size /max_dim)
                dest_w = int(img.shape[1] * size /max_dim)
                img = cv2.resize(img,(dest_w,dest_h))
                mask = cv2.resize(mask,(dest_w,dest_h))

            (h, w) = img.shape[:2]

            img_bg[(size-h)//2 : (size-h)//2 + h, (size-w)//2 : (size-w)//2 + w] = img

            img_bg = img_preproc_func(img_bg)
            mask = mask_preproc_func(mask)

            x_batch[0] = img_bg
            y_batch = mask
            yield x_batch, y_batch


########## Data generator is now threadsafe and should work with multiple workers ##########

import keras.backend as K

def iou(y_true, y_pred):
    smooth = 0.001

    y_true_f = K.flatten(K.round(y_true))
    y_pred_f = K.flatten(K.round(y_pred))

    intersection = K.sum(y_true_f * y_pred_f)
    union = K.sum(y_true_f) + K.sum(y_pred_f) - intersection
    score = (intersection + smooth) / (union + smooth)
    return score


def as_keras_metric(method):
    import functools
    from keras import backend as K
    import tensorflow as tf
    @functools.wraps(method)
    def wrapper(self, args, **kwargs):
        """ Wrapper for turning tensorflow metrics into keras metrics """
        value, update_op = method(self, args, **kwargs)
        K.get_session().run(tf.local_variables_initializer())
        with tf.control_dependencies([update_op]):
            value = tf.identity(value)
        return value
    return wrapper


def precision(y_true, y_pred):
    smooth = 0.001

    y_true_f = K.flatten(K.round(y_true))
    y_pred_f = K.flatten(K.round(y_pred))

    TP = K.sum(y_true_f * y_pred_f)
    FP = K.sum((1-y_true_f) * y_pred_f)
    return (TP+smooth)/(TP+FP+smooth)


def recall(y_true, y_pred):

    smooth = 0.001

    y_true_f = K.flatten(K.round(y_true))
    y_pred_f = K.flatten(K.round(y_pred))

    TP = K.sum(y_true_f * y_pred_f)
    FN = K.sum(y_true_f * (1-y_pred_f))
    return (TP+smooth)/(TP+FN+smooth)



def dice_loss(y_true, y_pred):
    """Soft dice (Sørensen or Jaccard) coefficient for comparing the similarity
    of two batch of data, usually be used for binary image segmentation
    i.e. labels are binary. The coefficient between 0 to 1, 1 means totally match.

    Parameters
    -----------
    output : Tensor
        A distribution with shape: [batch_size, ....], (any dimensions).
    target : Tensor
        The target distribution, format the same with `output`.
    loss_type : str
        ``jaccard`` or ``sorensen``, default is ``jaccard``.
    axis : tuple of int
        All dimensions are reduced, default ``[1,2,3]``.
    smooth : float
        This small value will be added to the numerator and denominator.
            - If both output and target are empty, it makes sure dice is 1.
            - If either output or target are empty (all pixels are background), dice = ```smooth/(small_value + smooth)``, then if smooth is very small, dice close to 0 (even the image values lower than the threshold), so in this case, higher smooth can have a higher dice.

    Examples
    ---------
    # outputs = tl.act.pixel_wise_softmax(network.outputs)
    # dice_loss = 1 - tl.cost.dice_coe(outputs, y_)

    References
    -----------
    - `Wiki-Dice <https://en.wikipedia.org/wiki/Sørensen–Dice_coefficient>`__

    """
    smooth = 0.00001

    inse = K.sum(y_pred * y_true)
    l = K.sum(y_pred * y_pred)
    r = K.sum(y_true * y_true)

    dice = (2. * inse + smooth) / (l + r + smooth)

    return 1-dice




from keras.callbacks import TensorBoard

class log_lr(Callback):
    def on_epoch_end(self, epoch, logs=None):
        logs = logs or {}
        logs['lr'] = K.get_value(self.model.optimizer.lr)

def get_callbacks(model_name, log_dir="/tmp/tensor_board", patience=30, others=None, hdf5_dir='weigths'):
    es = EarlyStopping(monitor='val_iou', patience=patience, mode="max")
    msave = ModelCheckpoint(os.path.join(hdf5_dir,model_name+'.hdf5'), monitor='val_iou',
                            save_best_only=True, save_weights_only=True, mode="max")
    log = TensorBoard(log_dir=log_dir+"/"+model_name)
    if others is None:
        return [es, msave, log_lr(), log]
    else:
        return [es, msave] + others + [log_lr(), log]

def get_run():
    if os.path.exists("runcount.csv"):
        rc = pd.read_csv("runcount.csv")
    else:
        rc=pd.DataFrame(data={'run':['0']}, dtype='int')
    run = rc.run[0]
    run += 1
    rc.run = [run]
    rc.to_csv("runcount.csv", index=False)
    return run

def eval_jaccard(y_true, y_pred):
    smooth = 1

    y_true_f = y_true.flatten()
    y_pred_f = y_pred.flatten()

    y_pred_f[y_pred_f>.5]=1
    y_pred_f[y_pred_f<=.5]=0

    intersection = np.sum(y_true_f * y_pred_f)
    union = np.sum(y_true_f) + np.sum(y_pred_f) - intersection
    score = (intersection + smooth) / (union + smooth)
    return score

def eval_acc(y_true, y_pred):

    y_true_f = y_true.flatten()
    y_pred_f = y_pred.flatten()

    y_pred_f[y_pred_f>.5]=1
    y_pred_f[y_pred_f<=.5]=0

    score = len(y_true_f[y_true_f==y_pred_f])/len(y_true_f)
    return score


def eval_TP(y_true, y_pred):
    #y_true_f = np.round(y_true.flatten())

    y_true_f = y_true.flatten()
    y_pred_f = y_pred.flatten()
    #y_pred_f[y_pred_f>.5]=1.0
    #y_pred_f[y_pred_f<=.5]=0.0

    return len(y_true_f[(y_true_f >= .5) & (y_pred_f >= .5)])


def eval_FP(y_true, y_pred):
    #y_true_f = np.round(y_true.flatten())
    y_true_f = y_true.flatten()
    y_pred_f = y_pred.flatten()
    #y_pred_f[y_pred_f > .5] = 1.0
    #y_pred_f[y_pred_f <= .5] = 0.0

    return len(y_true_f[(y_true_f < .5) & (y_pred_f >= .5)])

def eval_FN(y_true, y_pred):
    #y_true_f = np.round(y_true.flatten())
    y_true_f = y_true.flatten()
    y_pred_f = y_pred.flatten()
    #y_pred_f[y_pred_f > .5] = 1.0
    #y_pred_f[y_pred_f <= .5] = 0.0

    return len(y_true_f[(y_true_f >= .5) & (y_pred_f < .5)])



def eval_precision(y_true, y_pred):

    epsilon = 0.000001

    TP = eval_TP(y_true, y_pred)
    FP = eval_FP(y_true, y_pred)
    return (TP+epsilon)/(TP+FP+epsilon)

def eval_recall(y_true, y_pred):

    epsilon = 0.000001

    TP = eval_TP(y_true, y_pred)
    FN = eval_FN(y_true, y_pred)

    return (TP+epsilon)/(TP+FN+epsilon)


def get_images(dataset, image_dir):

    if dataset == "compaq":

        ori_path = os.path.join(image_dir, 'compaq/skin-images')
        gt_path = os.path.join(image_dir, 'compaq/masks')

        img_files = [os.path.basename(x) for x in glob.glob(ori_path + "/*.jpg")]
        img_files.sort()

        imgs = [os.path.join(ori_path, x) for x in  img_files]
        gts = [os.path.join(gt_path, x[:-4] + ".pbm") for x in  img_files]

    elif dataset == "sfa":

        ori_path = os.path.join(image_dir, 'sfa/ORI')

        gt_path = os.path.join(image_dir, 'sfa/GT')

        img_files = os.listdir(ori_path)
        img_files.sort()

        imgs = [os.path.join(ori_path, x) for x in img_files]

        gts = [os.path.join(gt_path, x) for x in img_files]

    elif dataset == "pratheepan":

        ori_path1 = os.path.join(image_dir, 'pratheepan/Pratheepan_Dataset/FacePhoto')
        ori_path2 = os.path.join(image_dir, 'pratheepan/Pratheepan_Dataset/FamilyPhoto')
        gt_path1 = os.path.join(image_dir, 'pratheepan/Ground_Truth/GroundT_FacePhoto')
        gt_path2 = os.path.join(image_dir, 'pratheepan/Ground_Truth/GroundT_FamilyPhoto')

        img_files1 = [os.path.basename(x) for x in glob.glob(ori_path1 + "/*.jpg")]
        img_files1.sort()

        img_files2 = [os.path.basename(x) for x in glob.glob(ori_path2 + "/*.jpg")]
        img_files2.sort()

        imgs = [os.path.join(ori_path1, x) for x in img_files1] + [os.path.join(ori_path2, x) for x in img_files2]
        gts = [os.path.join(gt_path1, x[:-4] + ".png") for x in img_files1] + [os.path.join(gt_path2, x[:-4] + ".png")
                                                                               for x in img_files2]


    elif dataset == "vpu":

        vpu_sets = ['AMI', 'ED', 'LIRIS', 'SSG', 'UT']

        imgs = []
        gts = []

        for set in vpu_sets:
            ori_path = os.path.join(image_dir, 'vpu/train/train_'+ set + '/raw')
            img_files = [os.path.basename(x) for x in glob.glob(ori_path + "/*.png")]
            img_files.sort()

            imgs = imgs + [os.path.join(ori_path, x) for x in img_files]
            gt_path = os.path.join(image_dir, 'vpu/train/train_'+ set + '/ann')

            if set == 'ED':
                gts = gts + [os.path.join(gt_path, x[3:-4] + ".png") for x in img_files]
            else:
                gts = gts + [os.path.join(gt_path, x[:-4] + ".png") for x in img_files]

            ori_path = os.path.join(image_dir, 'vpu/test/test_'+ set + '/raw')
            img_files = [os.path.basename(x) for x in glob.glob(ori_path + "/*.png")]
            img_files.sort()

            imgs = imgs + [os.path.join(ori_path, x) for x in img_files]
            gt_path = os.path.join(image_dir, 'vpu/test/test_'+ set + '/ann')

            if set == 'SSG':
                gts = gts + [os.path.join(gt_path, x[:-7] + "an.png") for x in img_files]
            else:
                gts = gts + [os.path.join(gt_path, x[:-4] + ".png") for x in img_files]

    else:

        print("Invalid source dataset")
        exit(-1)

    if len(imgs)!=len(gts):
        print("Image and ground truth lengths differ...")
        exit(-1)

    return imgs, gts


def split(imgs, gts, test_p = 0.15, valid_p=0.15):
    if test_p + valid_p > 1.0:
        print ("Test proportion + Valid proportion > 1.0!")
        exit(-1)

    if test_p == 1.0:

        print("Spliting in test only mode!")
        imgs, gts = shuffle(imgs, gts)

        return [],[],[],[],imgs, gts

    print("Spliting in %d-%d-%d" % ((1.0-(test_p+valid_p))*100, valid_p*100, test_p*100))
    dataset_size = len(imgs)
    test_size = int(dataset_size * test_p)
    valid_size = int(dataset_size * valid_p)



    x_img, test_img, x_gt, test_gt = train_test_split(imgs, gts, test_size=test_size, random_state=111)

    train_img, valid_img, train_gt, valid_gt = train_test_split(x_img, x_gt, test_size=valid_size, random_state=111)

    return  train_img, train_gt, valid_img, valid_gt, test_img, test_gt


def get_images_pseudo(dataset, image_dir, pseudo_dir, pseudo_model, train_set):

    train_set = [os.path.basename(x) for x in train_set]

    pseudo_dir = os.path.join(pseudo_dir, pseudo_model)

    if dataset == "compaq":

        ori_path = os.path.join(image_dir, 'compaq/skin-images')
        gt_path = os.path.join(pseudo_dir, 'compaq/skin-images')

        img_files = [os.path.basename(x) for x in glob.glob(ori_path + "/*.jpg")]
        img_files = [x for x in img_files if x not in train_set]
        img_files.sort()

        imgs = [os.path.join(ori_path, x) for x in  img_files]
        gts = [os.path.join(gt_path, x) for x in  img_files]

    elif dataset == "sfa":

        ori_path = os.path.join(image_dir, 'sfa/ORI')

        gt_path = os.path.join(pseudo_dir, 'sfa/ORI')

        img_files = os.listdir(ori_path)
        img_files = [x for x in img_files if x not in train_set]
        img_files.sort()

        imgs = [os.path.join(ori_path, x) for x in img_files]

        gts = [os.path.join(gt_path, x) for x in img_files]

    elif dataset == "pratheepan":

        ori_path1 = os.path.join(image_dir, 'pratheepan/Pratheepan_Dataset/FacePhoto')
        ori_path2 = os.path.join(image_dir, 'pratheepan/Pratheepan_Dataset/FamilyPhoto')
        gt_path1 = os.path.join(pseudo_dir, 'pratheepan/Pratheepan_Dataset/FacePhoto')
        gt_path2 = os.path.join(pseudo_dir, 'pratheepan/Pratheepan_Dataset/FamilyPhoto')

        img_files1 = [os.path.basename(x) for x in glob.glob(ori_path1 + "/*.jpg")]
        img_files1 = [x for x in img_files1 if x not in train_set]
        img_files1.sort()

        img_files2 = [os.path.basename(x) for x in glob.glob(ori_path2 + "/*.jpg")]
        img_files2 = [x for x in img_files2 if x not in train_set]
        img_files2.sort()

        imgs = [os.path.join(ori_path1, x) for x in img_files1] + [os.path.join(ori_path2, x) for x in img_files2]
        gts = [os.path.join(gt_path1, x) for x in img_files1] + [os.path.join(gt_path2, x) for x in img_files2]


    elif dataset == "vpu":

        vpu_sets = ['AMI', 'ED', 'LIRIS', 'SSG', 'UT']

        imgs = []
        gts = []

        for set in vpu_sets:
            ori_path = os.path.join(image_dir, 'vpu/train/train_'+ set + '/raw')
            img_files = [os.path.basename(x) for x in glob.glob(ori_path + "/*.png")]
            img_files = [x for x in img_files if x not in train_set]
            img_files.sort()

            imgs = imgs + [os.path.join(ori_path, x) for x in img_files]
            gt_path = os.path.join(pseudo_dir, 'vpu/train/train_' + set + '/raw')

            gts = gts + [os.path.join(gt_path, x) for x in img_files]

            ori_path = os.path.join(image_dir, 'vpu/test/test_' + set + '/raw')
            img_files = [os.path.basename(x) for x in glob.glob(ori_path + "/*.png")]
            img_files = [x for x in img_files if x not in train_set]
            img_files.sort()

            imgs = imgs + [os.path.join(ori_path, x) for x in img_files]
            gt_path = os.path.join(image_dir, 'vpu/test/test_' + set + '/raw')

            gts = gts + [os.path.join(gt_path, x) for x in img_files]

    else:

        print("Invalid source dataset")
        exit(-1)

    if len(imgs)!=len(gts):
        print("Image and ground truth lengths differ...")
        exit(-1)

    return imgs, gts


def split_pseudo(imgs, gts, valid_p=.02):

    dataset_size = len(imgs)
    valid_size = int(dataset_size * valid_p)

    train_img, valid_img, train_gt, valid_gt = train_test_split(imgs, gts, test_size=valid_size, random_state=111)

    return  train_img, train_gt, valid_img, valid_gt

def split_cross(imgs, gts, test_p = 0.15, valid_p=0.2, train_p=1):

    from math import ceil

    dataset_size = len(imgs)
    test_size = int(dataset_size * test_p)

    print(test_p)
    print(test_size)
    print(dataset_size)

    x_img, test_img, x_gt, test_gt = train_test_split(imgs, gts, test_size=test_size, random_state=111)

    if train_p == 1:
        valid_size = int(len(x_img) * valid_p)
        train_img, valid_img, train_gt, valid_gt = train_test_split(x_img, x_gt, test_size=valid_size, random_state=111)
    elif train_p == 0:
        train_img, valid_img, train_gt, valid_gt = [], [], [], []
    else:
        print(int(ceil(len(x_img)*train_p*valid_p)))
        x_img, _, x_gt, _ = train_test_split(x_img, x_gt, train_size=train_p, random_state=111)
        train_img, valid_img, train_gt, valid_gt = train_test_split(x_img, x_gt, test_size=int(ceil(len(x_img)*valid_p)), random_state=111)

    print("train %d valid %d test %d" % ( len(train_img), len(valid_img), len(test_img)))

    return train_img, train_gt, valid_img, valid_gt, test_img, test_gt

