#Source PRATHEEPAN ########################################################################################################################################

############
#COMPAQ
############

#target Only
#python train_cross.py unet pratheepan --epochs=100 --patience=20 --lr=0.0005 --decay=0.01 --train_proportion=0.05

#python train_cross.py unet pratheepan --epochs=100 --patience=20 --lr=0.0005 --decay=0.01 --train_proportion=.1

#python train_cross.py unet pratheepan --epochs=100 --patience=20 --lr=0.0005 --decay=0.01 --train_proportion=.5

#python train_cross.py unet pratheepan --epochs=100 --patience=20 --lr=0.0005 --decay=0.01 --train_proportion=1


#Fine Only

#python train_cross.py unet pratheepan --weights=unet_compaq_LR0.0001_DC0.002_11 --epochs=100 --patience=20 --lr=0.00001 --decay=0.002 --train_proportion=.05

#python train_cross.py unet pratheepan --weights=unet_compaq_LR0.0001_DC0.002_11 --epochs=100 --patience=20 --lr=0.00001 --decay=0.002 --train_proportion=.1

#python train_cross.py unet pratheepan --weights=unet_compaq_LR0.0001_DC0.002_11 --epochs=100 --patience=20 --lr=0.00001 --decay=0.002 --train_proportion=.50

#python train_cross.py unet pratheepan --weights=unet_compaq_LR0.0001_DC0.002_11 --epochs=100 --patience=20 --lr=0.00001 --decay=0.002 --train_proportion=1



#Pseudo Only
#python train_cross.py unet pratheepan --pseudo_model=unet_compaq_LR0.0001_DC0.002_11 --epochs=100 --patience=20 --lr=0.0005 --decay=0.01 --train_proportion=0

#python train_cross.py unet pratheepan --pseudo_model=unet_compaq_LR0.0001_DC0.002_11 --epochs=100 --patience=20 --lr=0.0005 --decay=0.01 --train_proportion=0.05

#python train_cross.py unet pratheepan --pseudo_model=unet_compaq_LR0.0001_DC0.002_11 --epochs=100 --patience=20 --lr=0.0005 --decay=0.01 --train_proportion=.1

#python train_cross.py unet pratheepan --pseudo_model=unet_compaq_LR0.0001_DC0.002_11 --epochs=100 --patience=20 --lr=0.0005 --decay=0.01 --train_proportion=.5


#Combined
#python pseudo_labels.py unet  unet_pratheepan_P_LR0.0005_DC0.01_265 pratheepan
#python train_cross.py unet pratheepan --pseudo_model=unet_pratheepan_P_LR0.0005_DC0.01_265 --weights=unet_pratheepan_P_LR0.0005_DC0.01_265 --epochs=100 --patience=30 --lr=0.0001 --#decay=0.002 --train_proportion=.0

#python pseudo_labels.py unet  unet_pratheepan_P_LR0.0005_DC0.01_266 pratheepan
#python train_cross.py unet pratheepan --pseudo_model=unet_pratheepan_P_LR0.0005_DC0.01_266 --weights=unet_pratheepan_P_LR0.0005_DC0.01_266 --epochs=100 --patience=30 --lr=0.0001 --#decay=0.002 --train_proportion=0.05

#python pseudo_labels.py unet  unet_pratheepan_P_LR0.0005_DC0.01_246 pratheepan
#python train_cross.py unet pratheepan --pseudo_model=unet_pratheepan_P_LR0.0005_DC0.01_246 --weights=unet_pratheepan_P_LR0.0005_DC0.01_246 --epochs=100 --patience=30 --lr=0.0001 --#decay=0.002 --train_proportion=0.1

#python pseudo_labels.py unet  unet_pratheepan_P_LR0.0005_DC0.01_245 pratheepan
#python train_cross.py unet pratheepan --pseudo_model=unet_pratheepan_P_LR0.0005_DC0.01_245 --weights=unet_pratheepan_P_LR0.0005_DC0.01_245 --epochs=100 --patience=30 --lr=0.0001 --#decay=0.002 --train_proportion=.5

############
#SFA
############

#target Only

#python train_cross.py unet sfa --epochs=100 --patience=20 --lr=0.0002 --decay=0.002 --train_proportion=0.05
#python train_cross.py unet sfa --epochs=100 --patience=20 --lr=0.0002 --decay=0.002 --train_proportion=0.1
#python train_cross.py unet sfa --epochs=100 --patience=20 --lr=0.0002 --decay=0.002 --train_proportion=0.5
#python train_cross.py unet sfa --epochs=100 --patience=20 --lr=0.0002 --decay=0.002 --train_proportion=1

#fine tuning Only

#python train_cross.py unet sfa --weights=unet_compaq_LR0.0001_DC0.002_11 --epochs=100 --patience=30 --lr=0.00008 --decay=0.001 --train_proportion=0.05
#python train_cross.py unet sfa --weights=unet_compaq_LR0.0001_DC0.002_11 --epochs=100 --patience=30 --lr=0.00008 --decay=0.001 --train_proportion=0.1
#python train_cross.py unet sfa --weights=unet_compaq_LR0.0001_DC0.002_11 --epochs=100 --patience=30 --lr=0.00008 --decay=0.001 --train_proportion=0.5
#python train_cross.py unet sfa --weights=unet_compaq_LR0.0001_DC0.002_11 --epochs=100 --patience=30 --lr=0.00008 --decay=0.001 --train_proportion=1


#Pseudo Only
#python train_cross.py unet sfa --pseudo_model=unet_compaq_LR0.0001_DC0.002_11 --epochs=100 --patience=20 --lr=0.0005 --decay=0.002 --train_proportion=0

#python train_cross.py unet sfa --pseudo_model=unet_compaq_LR0.0001_DC0.002_11 --epochs=100 --patience=20 --lr=0.0002 --decay=0.002 --train_proportion=0.05

#python train_cross.py unet sfa --pseudo_model=unet_compaq_LR0.0001_DC0.002_11 --epochs=100 --patience=20 --lr=0.0002 --decay=0.002 --train_proportion=.1

#python train_cross.py unet sfa --pseudo_model=unet_compaq_LR0.0001_DC0.002_11 --epochs=100 --patience=20 --lr=0.0002 --decay=0.002 --train_proportion=.5

