########################################################################
Same Domain
########################################################################



#UNET

Evaluation Results of model over dataset sfa
Acc: 0.9794  IOU: 0.9280  Prec: 0.9665  Recall: 0.9589  F-Score: 0.9627

Evaluation Results of model src_compaq_LR0.0001_DC0.002_11 over dataset compaq
Acc: 0.9262  IOU: 0.5447  Prec: 0.6849  Recall: 0.7164  F-Score: 0.7003

Evaluation Results of model unet_vpu_LR0.0002_DC0.005_20 over dataset vpu
Acc: 0.9903  IOU: 0.4529  Prec: 0.5786  Recall: 0.7133  F-Score: 0.6390

Evaluation Results of model unet_pratheepan_LR0.0001_DC0.002_12 over dataset pratheepan
Acc: 0.9175  IOU: 0.6043  Prec: 0.7291  Recall: 0.7451  F-Score: 0.7370

#PATCH

Evaluation Results of model patch_sfa_LR0.0001_DC0.002_27 over dataset sfa
Acc: 0.9414  IOU: 0.8217  Prec: 0.8971  Recall: 0.9100  F-Score: 0.9035

Evaluation Results of model patch_compaq_LR9e-05_DC0.002_42 over dataset compaq
Acc: 0.9018  IOU: 0.4600  Prec: 0.5892  Recall: 0.7359  F-Score: 0.6545

Evaluation Results of model patch_pratheepan_LR9e-05_DC0.002_34 over dataset pratheepan
Acc: 0.8712  IOU: 0.5557  Prec: 0.5983  Recall: 0.8249  F-Score: 0.6936

Evaluation Results of model patch_vpu_LR9e-05_DC0.002_35 over dataset vpu
Acc: 0.9348  IOU: 0.1414  Prec: 0.4634  Recall: 0.4282  F-Score: 0.4451




########################################################################
Cross-Domain
########################################################################

Evaluation Results of model unet_sfa_LR0.0001_DC0.002_10 over dataset compaq
Acc: 0.8478  IOU: 0.0695  Prec: 0.9056  Recall: 0.0878  F-Score: 0.1601

Evaluation Results of model unet_sfa_LR0.0001_DC0.002_10 over dataset pratheepan
Acc: 0.8552  IOU: 0.2051  Prec: 0.7537  Recall: 0.2442  F-Score: 0.3688

Evaluation Results of model unet_sfa_LR0.0001_DC0.002_10 over dataset vpu
Acc: 0.9102  IOU: 0.0074  Prec: 0.6538  Recall: 0.0904  F-Score: 0.1589

Evaluation Results of model unet_compaq_LR0.0001_DC0.002_11 over dataset sfa
Acc: 0.9172  IOU: 0.7467  Prec: 0.9701  Recall: 0.7636  F-Score: 0.8546

Evaluation Results of model unet_compaq_LR0.0001_DC0.002_11 over dataset pratheepan
Acc: 0.9259  IOU: 0.5943  Prec: 0.8089  Recall: 0.6750  F-Score: 0.7359

Evaluation Results of model unet_compaq_LR0.0001_DC0.002_11 over dataset vpu
Acc: 0.9674  IOU: 0.1317  Prec: 0.4177  Recall: 0.2196  F-Score: 0.2878

Evaluation Results of model unet_pratheepan_LR0.0001_DC0.002_12 over dataset sfa
Acc: 0.8902  IOU: 0.6627  Prec: 0.9498  Recall: 0.6902  F-Score: 0.7995

Evaluation Results of model unet_pratheepan_LR0.0001_DC0.002_12 over dataset compaq
Acc: 0.8932  IOU: 0.4496  Prec: 0.6010  Recall: 0.6773  F-Score: 0.6369

Evaluation Results of model unet_pratheepan_LR0.0001_DC0.002_12 over dataset vpu
Acc: 0.9684  IOU: 0.1722  Prec: 0.5905  Recall: 0.3146  F-Score: 0.4105

Evaluation Results of model unet_vpu_LR0.0002_DC0.005_20 over dataset sfa
Acc: 0.7211  IOU: 0.0770  Prec: 0.8552  Recall: 0.0784  F-Score: 0.1436

Evaluation Results of model unet_vpu_LR0.0002_DC0.005_20 over dataset compaq
Acc: 0.8515  IOU: 0.2405  Prec: 0.5663  Recall: 0.3598  F-Score: 0.4400

Evaluation Results of model unet_vpu_LR0.0002_DC0.005_20 over dataset pratheepan
Acc: 0.8528  IOU: 0.3653  Prec: 0.6893  Recall: 0.4508  F-Score: 0.5451


Evaluation Results of model patch_sfa_LR0.0001_DC0.002_27 over dataset compaq
Acc: 0.8716  IOU: 0.3368  Prec: 0.5544  Recall: 0.5550  F-Score: 0.5547

Evaluation Results of model patch_sfa_LR0.0001_DC0.002_27 over dataset pratheepan
Acc: 0.8306  IOU: 0.4348  Prec: 0.5211  Recall: 0.7130  F-Score: 0.6021

Evaluation Results of model patch_sfa_LR0.0001_DC0.002_27 over dataset vpu
Acc: 0.9018  IOU: 0.0676  Prec: 0.4926  Recall: 0.1713  F-Score: 0.2542

Evaluation Results of model patch_compaq_LR9e-05_DC0.002_42 over dataset sfa
Acc: 0.8560  IOU: 0.5439  Prec: 0.9642  Recall: 0.5564  F-Score: 0.7056

Evaluation Results of model patch_compaq_LR9e-05_DC0.002_42 over dataset pratheepan
Acc: 0.8602  IOU: 0.5138  Prec: 0.6071  Recall: 0.7936  F-Score: 0.6879

Evaluation Results of model patch_compaq_LR9e-05_DC0.002_42 over dataset vpu
Acc: 0.9544  IOU: 0.0820  Prec: 0.6618  Recall: 0.1407  F-Score: 0.2321

###########################################
#Final SUPERVISED
###########################################

#SFA SUPERVISED

Evaluation Results of model unet_sfa_LR0.0002_DC0.005_45 over dataset sfa
Acc: 0.9676  IOU: 0.9003  Prec: 0.9432  Recall: 0.9524  F-Score: 0.9478

Evaluation Results of model unet_sfa_LR0.0002_DC0.005_46 over dataset sfa
Acc: 0.9728  IOU: 0.9135  Prec: 0.9565  Recall: 0.9532  F-Score: 0.9548

Evaluation Results of model unet_sfa_LR0.0002_DC0.005_47 over dataset sfa
Acc: 0.9742  IOU: 0.9153  Prec: 0.9509  Recall: 0.9607  F-Score: 0.9558

#COMPAQ SUPERVISED

Evaluation Results of model unet_compaq_LR0.0002_DC0.005_48 over dataset compaq
Acc: 0.9030  IOU: 0.4922  Prec: 0.6384  Recall: 0.7014  F-Score: 0.6684

Evaluation Results of model unet_compaq_LR0.0002_DC0.005_49 over dataset compaq
Acc: 0.8966  IOU: 0.5071  Prec: 0.5938  Recall: 0.7894  F-Score: 0.6778

Evaluation Results of model unet_compaq_LR0.0002_DC0.005_50 over dataset compaq
Acc: 0.9184  IOU: 0.5316  Prec: 0.6684  Recall: 0.7209  F-Score: 0.6937

#PRATHEEPAN SUPERVISED

Evaluation Results of model unet_pratheepan_LR0.0002_DC0.005_51 over dataset pratheepan
Acc: 0.6985  IOU: 0.2196  Prec: 0.3397  Recall: 0.4609  F-Score: 0.3912

Evaluation Results of model unet_pratheepan_LR0.0002_DC0.005_52 over dataset pratheepan
Acc: 0.7891  IOU: 0.4380  Prec: 0.4602  Recall: 0.8860  F-Score: 0.6058

Evaluation Results of model unet_pratheepan_LR0.0002_DC0.005_53 over dataset pratheepan
Acc: 0.8851  IOU: 0.5481  Prec: 0.6239  Recall: 0.8187  F-Score: 0.7081

#VPU SUPERVISED

Evaluation Results of model unet_vpu_LR0.0002_DC0.005_54 over dataset vpu
Acc: 0.9879  IOU: 0.3293  Prec: 0.6341  Recall: 0.4686  F-Score: 0.5389

Evaluation Results of model unet_vpu_LR0.0002_DC0.005_55 over dataset vpu
Acc: 0.9879  IOU: 0.4023  Prec: 0.5446  Recall: 0.6656  F-Score: 0.5990

Evaluation Results of model unet_vpu_LR0.0002_DC0.005_56 over dataset vpu
Acc: 0.9882  IOU: 0.4047  Prec: 0.6120  Recall: 0.6151  F-Score: 0.6135


###########################################
#FINAL SOURCE COMPAQ
###########################################

#FINE TUNING
#SFA

Evaluation Results of model unet_sfa_LR2e-05_DC0.005_(unet_compaq_LR0.0001_DC0.002_11)_82 over dataset sfa
Acc: 0.9617  IOU: 0.8825  Prec: 0.9305  Recall: 0.9454  F-Score: 0.9379

Evaluation Results of model unet_sfa_LR2e-05_DC0.005_(unet_compaq_LR0.0001_DC0.002_11)_83 over dataset sfa
Acc: 0.9682  IOU: 0.9004  Prec: 0.9464  Recall: 0.9491  F-Score: 0.9478

Evaluation Results of model unet_sfa_LR2e-05_DC0.005_(unet_compaq_LR0.0001_DC0.002_11)_84 over dataset sfa
Acc: 0.9705  IOU: 0.9048  Prec: 0.9460  Recall: 0.9542  F-Score: 0.9501

#pratheepan

Evaluation Results of model unet_pratheepan_LR2e-05_DC0.005_(unet_compaq_LR0.0001_DC0.002_11)_85 over dataset pratheepan
Acc: 0.9039  IOU: 0.5609  Prec: 0.7086  Recall: 0.7202  F-Score: 0.7144

Evaluation Results of model unet_pratheepan_LR2e-05_DC0.005_(unet_compaq_LR0.0001_DC0.002_11)_86 over dataset pratheepan
Acc: 0.9249  IOU: 0.6219  Prec: 0.7839  Recall: 0.7368  F-Score: 0.7596

Evaluation Results of model unet_pratheepan_LR2e-05_DC0.005_(unet_compaq_LR0.0001_DC0.002_11)_87 over dataset pratheepan
Acc: 0.9210  IOU: 0.6253  Prec: 0.7238  Recall: 0.7877  F-Score: 0.7544

Evaluation Results of model unet_vpu_LR2e-05_DC0.005_(unet_compaq_LR0.0001_DC0.002_11)_88 over dataset vpu
Acc: 0.9883  IOU: 0.3197  Prec: 0.6805  Recall: 0.4288  F-Score: 0.5261

Evaluation Results of model unet_vpu_LR2e-05_DC0.005_(unet_compaq_LR0.0001_DC0.002_11)_89 over dataset vpu
Acc: 0.9890  IOU: 0.3538  Prec: 0.6250  Recall: 0.5017  F-Score: 0.5566



Evaluation Results of model unet_vpu_LR2e-05_DC0.005_(unet_compaq_LR0.0001_DC0.002_11)_90 over dataset vpu
Acc: 0.9901  IOU: 0.4143  Prec: 0.6531  Recall: 0.5567  F-Score: 0.6011


#VPU

Evaluation Results of model unet_vpu_LR2e-05_DC0.005_(unet_compaq_LR0.0001_DC0.002_11)_88 over dataset vpu
Acc: 0.9883  IOU: 0.3197  Prec: 0.6805  Recall: 0.4288  F-Score: 0.5261

Evaluation Results of model unet_vpu_LR2e-05_DC0.005_(unet_compaq_LR0.0001_DC0.002_11)_89 over dataset vpu
Acc: 0.9890  IOU: 0.3538  Prec: 0.6250  Recall: 0.5017  F-Score: 0.5566

Evaluation Results of model unet_vpu_LR2e-05_DC0.005_(unet_compaq_LR0.0001_DC0.002_11)_90 over dataset vpu
Acc: 0.9901  IOU: 0.4143  Prec: 0.6531  Recall: 0.5567  F-Score: 0.6011


#PSEUDO ONLY
#SFA

0%
Evaluation Results of model unet_sfa_LR0.0002_DC0.005_57 over dataset sfa
Acc: 0.9320  IOU: 0.7940  Prec: 0.9676  Recall: 0.8156  F-Score: 0.8851

Evaluation Results of model unet_sfa_LR0.0001_DC0.002_202 over dataset sfa
Acc: 0.9288  IOU: 0.7840  Prec: 0.9680  Recall: 0.8043  F-Score: 0.8786

10%
Evaluation Results of model unet_sfa_LR0.0002_DC0.005_59 over dataset sfa
Acc: 0.9328  IOU: 0.7974  Prec: 0.9686  Recall: 0.8186  F-Score: 0.8873

Evaluation Results of model unet_sfa_LR5e-05_DC0.002_203 over dataset sfa
Acc: 0.9573  IOU: 0.8686  Prec: 0.9555  Recall: 0.9054  F-Score: 0.9298

30%
Evaluation Results of model unet_sfa_LR0.0002_DC0.005_60 over dataset sfa
Acc: 0.9376  IOU: 0.8140  Prec: 0.9727  Recall: 0.8333  F-Score: 0.8976

Evaluation Results of model unet_sfa_LR7e-05_DC0.002_204 over dataset sfa
Acc: 0.9596  IOU: 0.8803  Prec: 0.9557  Recall: 0.9180  F-Score: 0.9364

50%
Evaluation Results of model unet_sfa_LR0.0002_DC0.005_61 over dataset sfa
Acc: 0.9468  IOU: 0.8403  Prec: 0.9679  Recall: 0.8645  F-Score: 0.9133

Evaluation Results of model unet_sfa_LR9e-05_DC0.002_205 over dataset sfa
Acc: 0.9637  IOU: 0.8898  Prec: 0.9389  Recall: 0.9456  F-Score: 0.9422

#pratheepan
Evaluation Results of model unet_pratheepan_LR0.0002_DC0.005_62 over dataset pratheepan
Acc: 0.9140  IOU: 0.5794  Prec: 0.7437  Recall: 0.7203  F-Score: 0.7318

Evaluation Results of model unet_pratheepan_LR0.0001_DC0.002_206 over dataset pratheepan
Acc: 0.9225  IOU: 0.5843  Prec: 0.7861  Recall: 0.6802  F-Score: 0.7293

10%
Evaluation Results of model unet_pratheepan_LR0.0002_DC0.005_63 over dataset pratheepan
Acc: 0.8762  IOU: 0.5350  Prec: 0.6596  Recall: 0.7373  F-Score: 0.6963

Evaluation Results of model unet_pratheepan_LR0.0001_DC0.002_207 over dataset pratheepan
Acc: 0.7414  IOU: 0.3731  Prec: 0.4164  Recall: 0.7805  F-Score: 0.5431

30%
Evaluation Results of model unet_pratheepan_LR0.0002_DC0.005_64 over dataset pratheepan
Acc: 0.9095  IOU: 0.5965  Prec: 0.7393  Recall: 0.7461  F-Score: 0.7427

Evaluation Results of model unet_pratheepan_LR0.0001_DC0.002_208 over dataset pratheepan
Acc: 0.9030  IOU: 0.5500  Prec: 0.7281  Recall: 0.6831  F-Score: 0.7048

50%

Evaluation Results of model unet_pratheepan_LR0.0002_DC0.005_65 over dataset pratheepan
Acc: 0.9073  IOU: 0.6071  Prec: 0.6931  Recall: 0.7954  F-Score: 0.7407

Evaluation Results of model unet_pratheepan_LR0.0001_DC0.002_209 over dataset pratheepan
Acc: 0.9064  IOU: 0.5787  Prec: 0.6835  Recall: 0.7612  F-Score: 0.7203


#vpu
Evaluation Results of model unet_vpu_LR0.0002_DC0.005_66 over dataset vpu
Acc: 0.0157  IOU: 0.0141  Prec: 0.0141  Recall: 1.0000  F-Score: 0.0278

Evaluation Results of model unet_vpu_LR0.0002_DC0.005_210 over dataset vpu
Acc: 0.0155  IOU: 0.0141  Prec: 0.0141  Recall: 1.0000  F-Score: 0.0277

Evaluation Results of model unet_vpu_LR0.0002_DC0.005_67 over dataset vpu
Acc: 0.0166  IOU: 0.0137  Prec: 0.0137  Recall: 0.9999  F-Score: 0.0269

Evaluation Results of model unet_vpu_LR0.0002_DC0.005_211 over dataset vpu
Acc: 0.7803  IOU: 0.0444  Prec: 0.0465  Recall: 0.6300  F-Score: 0.0866



Evaluation Results of model unet_vpu_LR0.0002_DC0.005_68 over dataset vpu
Acc: 0.0175  IOU: 0.0142  Prec: 0.0142  Recall: 1.0000  F-Score: 0.0280
Evaluation Results of model unet_vpu_LR0.0002_DC0.005_212 over dataset vpu
Acc: 0.9077  IOU: 0.1363  Prec: 0.3115  Recall: 0.5428  F-Score: 0.3958

Evaluation Results of model unet_vpu_LR0.0002_DC0.005_69 over dataset vpu
Acc: 0.0262  IOU: 0.0151  Prec: 0.0151  Recall: 0.9996  F-Score: 0.0297

Evaluation Results of model unet_vpu_LR0.0002_DC0.005_213 over dataset vpu
Acc: 0.7432  IOU: 0.1286  Prec: 0.1657  Recall: 0.7497  F-Score: 0.2714


#COMBINED
#SFA
Evaluation Results of model unet_sfa_LR2e-05_DC0.005_(unet_compaq_LR0.0001_DC0.002_11)_70 over dataset sfa
Acc: 0.9337  IOU: 0.7998  Prec: 0.9667  Recall: 0.8225  F-Score: 0.8888

Evaluation Results of model unet_sfa_LR2e-05_DC0.005_(unet_compaq_LR0.0001_DC0.002_11)_71 over dataset sfa
Acc: 0.9371  IOU: 0.8109  Prec: 0.9669  Recall: 0.8342  F-Score: 0.8957

Evaluation Results of model unet_sfa_LR2e-05_DC0.005_(unet_compaq_LR0.0001_DC0.002_11)_72 over dataset sfa
Acc: 0.9410  IOU: 0.8244  Prec: 0.9671  Recall: 0.8484  F-Score: 0.9038

Evaluation Results of model unet_sfa_LR2e-05_DC0.005_(unet_compaq_LR0.0001_DC0.002_11)_73 over dataset sfa
Acc: 0.9478  IOU: 0.8414  Prec: 0.9655  Recall: 0.8677  F-Score: 0.9140

#PRATHEEPAN
Evaluation Results of model unet_pratheepan_LR2e-05_DC0.005_(unet_compaq_LR0.0001_DC0.002_11)_74 over dataset pratheepan
Acc: 0.9200  IOU: 0.5790  Prec: 0.7881  Recall: 0.6924  F-Score: 0.7371

Evaluation Results of model unet_pratheepan_LR2e-05_DC0.005_(unet_compaq_LR0.0001_DC0.002_11)_75 over dataset pratheepan
Acc: 0.9227  IOU: 0.5934  Prec: 0.7860  Recall: 0.6966  F-Score: 0.7386

Evaluation Results of model unet_pratheepan_LR2e-05_DC0.005_(unet_compaq_LR0.0001_DC0.002_11)_76 over dataset pratheepan
Acc: 0.9197  IOU: 0.6196  Prec: 0.7494  Recall: 0.7579  F-Score: 0.7536

Evaluation Results of model unet_pratheepan_LR2e-05_DC0.005_(unet_compaq_LR0.0001_DC0.002_11)_77 over dataset pratheepan
Acc: 0.9151  IOU: 0.6052  Prec: 0.7249  Recall: 0.7526  F-Score: 0.7385

#VPU
Evaluation Results of model unet_vpu_LR2e-05_DC0.005_(unet_compaq_LR0.0001_DC0.002_11)_78 over dataset vpu
Acc: 0.0174  IOU: 0.0141  Prec: 0.0141  Recall: 1.0000  F-Score: 0.0278

Evaluation Results of model unet_vpu_LR2e-05_DC0.005_(unet_compaq_LR0.0001_DC0.002_11)_79 over dataset vpu
Acc: 0.0205  IOU: 0.0137  Prec: 0.0137  Recall: 0.9990  F-Score: 0.0271

Evaluation Results of model unet_vpu_LR2e-05_DC0.005_(unet_compaq_LR0.0001_DC0.002_11)_80 over dataset vpu
Acc: 0.0567  IOU: 0.0147  Prec: 0.0147  Recall: 0.9827  F-Score: 0.0289

Evaluation Results of model unet_vpu_LR2e-05_DC0.005_(unet_compaq_LR0.0001_DC0.002_11)_81 over dataset vpu
Acc: 0.0832  IOU: 0.0153  Prec: 0.0153  Recall: 0.9394  F-Score: 0.0301



###########################################
#FINAL SOURCE PRATHEEPAN
###########################################

