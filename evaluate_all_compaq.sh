#python evaluate.py unet unet_sfa_LR0.0002_DC0.005_57 sfa  --test_proportion=1
#python evaluate.py unet unet_sfa_LR0.0002_DC0.005_59 sfa  --test_proportion=.9
#python evaluate.py unet unet_sfa_LR0.0002_DC0.005_60 sfa  --test_proportion=.7
#python evaluate.py unet unet_sfa_LR0.0002_DC0.005_61 sfa  --test_proportion=.5

#python evaluate.py unet unet_pratheepan_LR0.0002_DC0.005_62 pratheepan  --test_proportion=1
#python evaluate.py unet unet_pratheepan_LR0.0002_DC0.005_63 pratheepan  --test_proportion=.9
#python evaluate.py unet unet_pratheepan_LR0.0002_DC0.005_64 pratheepan  --test_proportion=.7
#python evaluate.py unet unet_pratheepan_LR0.0002_DC0.005_65 pratheepan  --test_proportion=.5

#python evaluate.py unet unet_vpu_LR0.0002_DC0.005_66 vpu  --test_proportion=1
#python evaluate.py unet unet_vpu_LR0.0002_DC0.005_67 vpu  --test_proportion=.9
#python evaluate.py unet unet_vpu_LR0.0002_DC0.005_68 vpu  --test_proportion=.7
#python evaluate.py unet unet_vpu_LR0.0002_DC0.005_69 vpu  --test_proportion=.5

python evaluate.py unet "unet_sfa_LR2e-05_DC0.005_(unet_compaq_LR0.0001_DC0.002_11)_70" sfa  --test_proportion=1
python evaluate.py unet "unet_sfa_LR2e-05_DC0.005_(unet_compaq_LR0.0001_DC0.002_11)_71" sfa  --test_proportion=.9
python evaluate.py unet "unet_sfa_LR2e-05_DC0.005_(unet_compaq_LR0.0001_DC0.002_11)_72" sfa  --test_proportion=.7
python evaluate.py unet "unet_sfa_LR2e-05_DC0.005_(unet_compaq_LR0.0001_DC0.002_11)_73" sfa  --test_proportion=.5

python evaluate.py unet "unet_pratheepan_LR2e-05_DC0.005_(unet_compaq_LR0.0001_DC0.002_11)_74" pratheepan  --test_proportion=1
python evaluate.py unet "unet_pratheepan_LR2e-05_DC0.005_(unet_compaq_LR0.0001_DC0.002_11)_75" pratheepan  --test_proportion=.9
python evaluate.py unet "unet_pratheepan_LR2e-05_DC0.005_(unet_compaq_LR0.0001_DC0.002_11)_76" pratheepan  --test_proportion=.7
python evaluate.py unet "unet_pratheepan_LR2e-05_DC0.005_(unet_compaq_LR0.0001_DC0.002_11)_77" pratheepan  --test_proportion=.5

python evaluate.py unet "unet_vpu_LR2e-05_DC0.005_(unet_compaq_LR0.0001_DC0.002_11)_78" vpu  --test_proportion=1
python evaluate.py unet "unet_vpu_LR2e-05_DC0.005_(unet_compaq_LR0.0001_DC0.002_11)_79" vpu  --test_proportion=.9
python evaluate.py unet "unet_vpu_LR2e-05_DC0.005_(unet_compaq_LR0.0001_DC0.002_11)_80" vpu  --test_proportion=.7
python evaluate.py unet "unet_vpu_LR2e-05_DC0.005_(unet_compaq_LR0.0001_DC0.002_11)_81" vpu  --test_proportion=.5

python evaluate.py unet "unet_sfa_LR2e-05_DC0.005_(unet_compaq_LR0.0001_DC0.002_11)_82" sfa  --test_proportion=.9
python evaluate.py unet "unet_sfa_LR2e-05_DC0.005_(unet_compaq_LR0.0001_DC0.002_11)_83" sfa  --test_proportion=.7
python evaluate.py unet "unet_sfa_LR2e-05_DC0.005_(unet_compaq_LR0.0001_DC0.002_11)_84" sfa  --test_proportion=.5

python evaluate.py unet "unet_pratheepan_LR2e-05_DC0.005_(unet_compaq_LR0.0001_DC0.002_11)_85" pratheepan  --test_proportion=.9
python evaluate.py unet "unet_pratheepan_LR2e-05_DC0.005_(unet_compaq_LR0.0001_DC0.002_11)_86" pratheepan  --test_proportion=.7
python evaluate.py unet "unet_pratheepan_LR2e-05_DC0.005_(unet_compaq_LR0.0001_DC0.002_11)_87" pratheepan  --test_proportion=.5

python evaluate.py unet "unet_vpu_LR2e-05_DC0.005_(unet_compaq_LR0.0001_DC0.002_11)_88" vpu  --test_proportion=.9
python evaluate.py unet "unet_vpu_LR2e-05_DC0.005_(unet_compaq_LR0.0001_DC0.002_11)_89" vpu  --test_proportion=.7
python evaluate.py unet "unet_vpu_LR2e-05_DC0.005_(unet_compaq_LR0.0001_DC0.002_11)_90" vpu  --test_proportion=.5


