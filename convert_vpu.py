import os
os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"   # see issue #152
os.environ["CUDA_VISIBLE_DEVICES"]="1"
import numpy as np # linear algebra
import cv2
from dnn_utils import get_images



##################################
IMAGE_DIR = "/d02/data/skin/images"
DATASET = "vpu" #compaq, sfa, pratheepan, vpu
##################################


'''
def get_images():

    if DATASET == "compaq":

        ori_path = os.path.join(IMAGE_DIR, 'compaq/skin-images')
        gt_path = os.path.join(IMAGE_DIR, 'compaq/masks')

        img_files = [os.path.basename(x) for x in glob.glob(ori_path + "/*.jpg")]

        imgs =  [os.path.join(ori_path, x) for x in  img_files]
        gts =  [os.path.join(gt_path, x[:-4] + ".pbm") for x in  img_files]

    elif DATASET == "sfa":

        ori_path = os.path.join(IMAGE_DIR, 'sfa/ORI')

        gt_path = os.path.join(IMAGE_DIR, 'sfa/GT')

        img_files = os.listdir(ori_path)

        imgs = [os.path.join(ori_path, x) for x in img_files]

        gts = [os.path.join(gt_path, x) for x in img_files]

    elif DATASET == "pratheepan":

        ori_path1 = os.path.join(IMAGE_DIR, 'pratheepan/Pratheepan_Dataset/FacePhoto')
        ori_path2 = os.path.join(IMAGE_DIR, 'pratheepan/Pratheepan_Dataset/FamilyPhoto')
        gt_path1 = os.path.join(IMAGE_DIR, 'pratheepan/Ground_Truth/GroundT_FacePhoto')
        gt_path2 = os.path.join(IMAGE_DIR, 'pratheepan/Ground_Truth/GroundT_FamilyPhoto')

        img_files1 = [os.path.basename(x) for x in glob.glob(ori_path1 + "/*.jpg")]
        img_files2 = [os.path.basename(x) for x in glob.glob(ori_path2 + "/*.jpg")]

        imgs = [os.path.join(ori_path1, x) for x in img_files1] + [os.path.join(ori_path2, x) for x in img_files2]
        gts = [os.path.join(gt_path1, x[:-4] + ".png") for x in img_files1] + [os.path.join(gt_path2, x[:-4] + ".png")
                                                                               for x in img_files2]

    elif DATASET == "vpu":

        vpu_sets = ['AMI', 'ED', 'LIRIS', 'SSG', 'UT']

        imgs = []
        gts = []

        for set in vpu_sets:
            ori_path = os.path.join(IMAGE_DIR, 'vpu/train/train_'+ set + '/raw')
            img_files = [os.path.basename(x) for x in glob.glob(ori_path + "/*.png")]
            imgs = imgs + [os.path.join(ori_path, x) for x in img_files]

            gt_path = os.path.join(IMAGE_DIR, 'vpu/train/train_'+ set + '/ann')
            if set == 'ED':
                gts = gts + [os.path.join(gt_path, x[3:-4] + ".png") for x in img_files]
            else:
                gts = gts + [os.path.join(gt_path, x[:-4] + ".png") for x in img_files]

            ori_path = os.path.join(IMAGE_DIR, 'vpu/test/test_'+ set + '/raw')
            img_files = [os.path.basename(x) for x in glob.glob(ori_path + "/*.png")]
            imgs = imgs + [os.path.join(ori_path, x) for x in img_files]

            gt_path = os.path.join(IMAGE_DIR, 'vpu/test/test_'+ set + '/ann')

            if set == 'SSG':
                gts = gts + [os.path.join(gt_path, x[:-7] + "an.png") for x in img_files]
            else:
                gts = gts + [os.path.join(gt_path, x[:-4] + ".png") for x in img_files]

    else:

        print("Invalid source dataset")
        exit(-1)

    if len(imgs)!=len(gts):
        print("Image and ground truth lengths differ...")
        exit(-1)


    return imgs, gts
'''


imgs, gts = get_images(DATASET, IMAGE_DIR)

for i,g in zip(imgs,gts):

    print(i)
    print(g)
    img = cv2.imread(i)
    gt = cv2.imread(g)

    gt2 = np.copy(gt)

    for x in range(img.shape[0]):
        for y in range(img.shape[1]):
            if sum(img[x,y] == gt[x,y]) == 3:
                gt2[x,y] = [0,0,0]
            else:
                gt2[x,y] = img[x,y]

    cv2.imwrite(g, gt2)

