import os
os.environ["KERAS_BACKEND"]="tensorflow"
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"   # see issue #152
os.environ["CUDA_VISIBLE_DEVICES"]="0"

import numpy as np # linear algebra
from keras.optimizers import RMSprop
import cv2
from unet_512_768 import get_unet_512
from patch_net import get_patch_net2
from dnn_utils import get_images, skin_preprocess, mask_preprocess, dice_loss, iou
from dnn_utils import eval_jaccard, eval_acc, evaluation_generator, split, eval_precision, eval_recall, evaluate_patch_generator
import argparse
from tqdm import tqdm
import pandas as pd


##################################

BATCH_SIZE = 4
LR=0.0001
DECAY=0.002

IMAGE_DIR = "/d02/data/skin/images"
HDF5_DIR = "weights"
DATASET = "compaq" #compaq, sfa, pratheepan, vpu
TEST_PROPORTION = .15
TEST_BATCH_SIZE = 15
CFG_FILE = "pic.cfg"
ARCHITECTURE = 'unet'
SIZE=256
##################################


def evaluate():

    global TEST_PROPORTION

    imgs, gts = get_images(DATASET, IMAGE_DIR)

    if TEST_PROPORTION == 1:
        VALID_PROPORTION = 0
    elif TEST_PROPORTION < .5:
        VALID_PROPORTION = TEST_PROPORTION
    else:
        VALID_PROPORTION = (1 - TEST_PROPORTION)/2


    _, _, _, _, test_img, test_gt = split(imgs, gts, test_p = TEST_PROPORTION, valid_p=VALID_PROPORTION)

    if ARCHITECTURE == 'unet':
        model = get_unet_512()
        test_datagen = evaluation_generator(test_img, mask_files=test_gt, img_preproc_func=skin_preprocess,
        mask_preproc_func=mask_preprocess)

    elif ARCHITECTURE == 'patch':
        model = get_patch_net2()
        patch_datagen = evaluate_patch_generator(test_img, mask_files=test_gt, img_preproc_func=skin_preprocess,
                                                 mask_preproc_func=mask_preprocess)

    else:
        print("Invalid archtecture: ", ARCHITECTURE)
        exit(-1)

    model.compile(optimizer=RMSprop(lr=LR, decay=DECAY), loss=dice_loss, metrics=[iou, 'accuracy'])


    ################################################
    print ("\n\nEvaluating model", MODEL, "over dataset", DATASET )
    ################################################

    model.load_weights(os.path.join(HDF5_DIR, MODEL + ".hdf5"))

    TEST_STEPS = len(test_img)
    SIZE = 768

    # N_THUMBS = 28 * 4
    N_THUMBS = 28

    # FACTOR = 8
    FACTOR = 4

    font = cv2.FONT_HERSHEY_SIMPLEX

    acc, jac, prec, recall = 0, 0, 0, 0

    tn_h, tn_w = 768 // FACTOR, 768 // FACTOR
    grid_size = (7, 12)

    im_result = np.zeros((tn_h * grid_size[0], tn_w * grid_size[1], 3), np.uint8)

    #TEST_STEPS = 10
    for i in tqdm(range(TEST_STEPS)):

        img = cv2.imread(test_img[i])

        if ARCHITECTURE == 'unet' :
            # UNet Prediction
            _img, mask = next(test_datagen)

            unet_prediction = model.predict(_img)[0]

            # fixing size
            (h, w) = mask.shape[:2]
            prediction = unet_prediction[(SIZE - h) // 2: (SIZE - h) // 2 + h, (SIZE - w) // 2: (SIZE - w) // 2 + w]

        else:
            patch, mask = next(patch_datagen)

            prediction = model.predict(patch)

        a = eval_acc(mask, prediction)
        j = eval_jaccard(mask, prediction)
        p = eval_precision(mask, prediction)
        r = eval_recall(mask, prediction)

        acc += a
        jac += j
        prec += p
        recall += r

        # Thumbnails

        if i < N_THUMBS:

            max_dim = max(img.shape[0], img.shape[1])
            dest_h = int((img.shape[0] * SIZE / max_dim) / FACTOR)
            dest_w = int((img.shape[1] * SIZE / max_dim) / FACTOR)

            img_th = cv2.resize(img, (dest_w, dest_h))
            mask_th = cv2.resize((mask*255).astype(np.uint8), (dest_w, dest_h))
            if ARCHITECTURE == 'unet':
                pred_th = cv2.resize((np.round(prediction) * 255).astype(np.uint8), (dest_w, dest_h))
            else:
                pred_th = cv2.resize((np.round(prediction.reshape((img.shape[0], img.shape[1]))) * 255).astype(np.uint8),
                                                (dest_w, dest_h))

            lin = (i * 3) // grid_size[1]
            col = (i * 3) % grid_size[1]

            y_start = lin * tn_h
            x_start = col * tn_w

            im_result[y_start:y_start + img_th.shape[0], x_start:x_start + img_th.shape[1]] = img_th

            for channel in range(3):
                im_result[y_start:y_start + img_th.shape[0], x_start + tn_w:x_start + tn_w + img_th.shape[1],
                channel] = mask_th
                im_result[y_start:y_start + img_th.shape[0], x_start + 2 * tn_w:x_start + 2 * tn_w + img_th.shape[1],
                channel] = pred_th

            for channel in range(3):
                im_result[y_start:y_start + img_th.shape[0], x_start + tn_w:x_start + tn_w + img_th.shape[1],
                channel] = mask_th

                im_result[y_start:y_start + img_th.shape[0], x_start + 2 * tn_w:x_start + 2 * tn_w + img_th.shape[1],
                channel] = pred_th
                cv2.putText(im_result, 'acc={:.2f}'.format(a), (x_start + 2 * tn_w, y_start + 15), font, 0.5,
                            (0, 255, 0), 1, cv2.LINE_AA)
                cv2.putText(im_result, 'iou={:.2f}'.format(j), (x_start + 2 * tn_w, y_start + 35), font, 0.5,
                            (0, 255, 0), 1, cv2.LINE_AA)

            #cv2.imshow("Result", im_result)
            #c = cv2.waitKey(3)
        elif i == N_THUMBS:
            #cv2.imshow("Result", im_result)
            #cv2.destroyAllWindows()
            cv2.imwrite("results/evaluate_"+MODEL+"_"+DATASET+".png", im_result)

    acc, jac = acc / TEST_STEPS, jac / TEST_STEPS
    prec, recall = prec / TEST_STEPS, recall / TEST_STEPS


    print ("\nEvaluation Results of model", MODEL, "over dataset", DATASET )
    print("Acc: %.4f  IOU: %.4f  Prec: %.4f  Recall: %.4f  F-Score: %.4f" %
          (acc, jac, prec, recall, 2*prec*recall/(prec+recall)))
    print("\npress q...")

    #cv2.imshow("Result", im_result)
    #while True:
    #    c = cv2.waitKey(3) & 0xFF
    #    if c in [ord('q'), ord('n'), ord('s')]:
    #        break
    #cv2.destroyAllWindows()


def get_img_gt(dataset, number):
    imgs, gts = get_images(dataset, IMAGE_DIR)

    if TEST_PROPORTION == 1:
        VALID_PROPORTION = 0
    elif TEST_PROPORTION < .5:
        VALID_PROPORTION = TEST_PROPORTION
    else:
        VALID_PROPORTION = (1 - TEST_PROPORTION) / 2
    _, _, _, _, test_img, test_gt = split(imgs, gts, test_p=TEST_PROPORTION, valid_p=VALID_PROPORTION)

    return test_img[number], test_gt[number]


def get_prediction(architecture, weights, img, gt):

    img_size =  768

    if architecture == 'unet':
        model = get_unet_512()
        model.compile(optimizer=RMSprop(lr=LR, decay=DECAY), loss=dice_loss, metrics=[iou, 'accuracy'])
        model.load_weights(os.path.join(HDF5_DIR, weights + ".hdf5"))
        test_datagen = evaluation_generator(img, mask_files=gt, img_preproc_func=skin_preprocess,
                                            mask_preproc_func=mask_preprocess)
        _img, mask = next(test_datagen)

        unet_prediction = model.predict(_img)[0]

        # fixing size
        (h, w) = mask.shape[:2]
        prediction = unet_prediction[(img_size - h) // 2: (img_size - h) // 2 + h, (img_size - w) // 2: (img_size - w) // 2 + w]

    elif ARCHITECTURE == 'patch':
        model = get_patch_net2()
        model.compile(optimizer=RMSprop(lr=LR, decay=DECAY), loss=dice_loss, metrics=[iou, 'accuracy'])
        model.load_weights(os.path.join(HDF5_DIR, weights + ".hdf5"))
        patch_datagen = evaluate_patch_generator(img, mask_files=gt, img_preproc_func=skin_preprocess,
                                                 mask_preproc_func=mask_preprocess)
        patch, mask = next(patch_datagen)

        prediction = model.predict(patch)

    else:
        print("Invalid archtecture: ", ARCHITECTURE)
        exit(-1)

    return prediction, mask

def fit(img):
    from math import ceil
    im_result = np.zeros((SIZE, SIZE, 3), np.uint8)
    max_dim = max(img.shape[0], img.shape[1])
    sy = int(ceil(SIZE * img.shape[0] / max_dim))
    sx = int(ceil(SIZE * img.shape[1] / max_dim))
    im_result[(SIZE-sy)//2:(SIZE-sy)//2 +sy   ,(SIZE-sx)//2:(SIZE-sx)//2+sx] = cv2.resize(img, (sx, sy))
    return im_result


def gt_preprocess(im):
    im = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
    im[im <  1] = 0
    im[im >= 1 ] = 255
    #im = im.reshape((im.shape[0],im.shape[1],1))
    #im = np.zeros((img.shape[0],img.shape[1],1),np.uint8)
    #im[(img[:,:,0] + img[:,:,1] + img[:,:,2]) >THRESHOLD] = 1
    im = cv2.cvtColor(im, cv2.COLOR_GRAY2BGR)
    return im


def process():

    font = cv2.FONT_HERSHEY_SIMPLEX

    df = pd.read_csv(CFG_FILE)

    x, y = 0, 0

    im_result = np.zeros((len(df)*SIZE, 5*SIZE, 3), np.uint8)

    for index, rec in df.iterrows():


        img_file, gt_file =  get_img_gt(rec.dataset, rec.number)

        print(img_file)

        img = cv2.imread(img_file)
        gt = cv2.imread(gt_file)



        im_result[y:(y+SIZE),x:(x+SIZE)] = fit(img)
        x += SIZE
        im_result[y:(y+SIZE),x:(x+SIZE)] = fit(gt_preprocess(gt))
        x += SIZE

        cv2.imshow("Result", im_result)
        c = cv2.waitKey(3)

        for i in np.array(range(3))+1:
            print(rec['model'+str(i)])

            prediction, mask = get_prediction(rec['architecture'+str(i)], rec['model'+str(i)],[img_file], [gt_file])

            j = eval_jaccard(mask, prediction)
            p = eval_precision(mask, prediction)
            r = eval_recall(mask, prediction)
            f = 2 * p * r / (p + r)

            if rec['architecture'+str(i)] == 'unet':
                pred_img = (np.round(prediction) * 255).astype(np.uint8).reshape((img.shape[0],img.shape[1],1))
            else:
                pred_img = (np.round(prediction.reshape((img.shape[0], img.shape[1],1))) * 255).astype(np.uint8)

            pred_img = cv2.cvtColor(pred_img,cv2.COLOR_GRAY2BGR)

            im_result[y:(y+SIZE),x:(x+SIZE)] = fit(pred_img)

            cv2.putText(im_result, 'f-score: {:.2f}'.format(f), (x+5, y+25), font, .8,
                        (0, 255, 0), 1, cv2.LINE_AA)

            x += SIZE

            cv2.imshow("Result", im_result)
            c = cv2.waitKey(3)


        x = 0
        y += SIZE

    cv2.destroyAllWindows()

    cv2.imwrite("results/" + CFG_FILE[:-4] + ".png", im_result)


# Main Function
def Run():

    global IMAGE_DIR, HDF5_DIR,  TEST_PROPORTION, CFG_FILE, SIZE

    parser = argparse.ArgumentParser(description='Generate output pictures.')
    parser.add_argument("--cfg_file", help="Config file", type=str, default=CFG_FILE, required=False)
    parser.add_argument("--size", help="thumb size", type=int, default=SIZE, required=False)
    parser.add_argument("--image_dir", help="Image directory", type=str, default=IMAGE_DIR,required=False)
    parser.add_argument("--hdf5_dir", help="HDF5 Directort", type=str, default=HDF5_DIR,required=False)
    parser.add_argument("--test_proportion", help="Test Proportion.", type=float, default=TEST_PROPORTION, required=False)

    args = parser.parse_args()

    CFG_FILE = args.cfg_file
    IMAGE_DIR = args.image_dir
    HDF5_DIR = args.hdf5_dir
    TEST_PROPORTION = args.test_proportion
    SIZE = args.size

    process()


if __name__ == '__main__':
  Run()





