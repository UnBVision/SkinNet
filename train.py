import os
os.environ["KERAS_BACKEND"]="tensorflow"
#os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"   # see issue #152
#os.environ["CUDA_VISIBLE_DEVICES"]="1"
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
from keras.optimizers import RMSprop
from dnn_utils import segmentation_generator, patch_generator, evaluate_patch_generator
from dnn_utils import skin_preprocess, mask_preprocess, dice_loss
from dnn_utils import get_images, iou, precision, recall, get_run, get_callbacks,split
from math import ceil

import argparse


##################################
BATCH_SIZE = 4
LR=0.0001
DECAY=0.002

IMAGE_DIR = "/d02/data/skin/images"
HDF5_DIR = "weights"
SOURCE_DATASET = "compaq" #compaq or sfa
TEST_PROPORTION = 0.15
VALID_PROPORTION = 0.15
EPOCHS = 100
PATIENCE = 10
ARCHITECTURE = 'unet'
WEIGHTS = 'none'

def train():
    imgs, gts = get_images(SOURCE_DATASET, IMAGE_DIR)

    train_img, train_gt, valid_img, valid_gt, test_img, test_gt = split(imgs, gts, test_p=TEST_PROPORTION, valid_p=VALID_PROPORTION)

    if ARCHITECTURE == 'unet':
        TEST_BATCH_SIZE = 15
        from unet_512_768 import get_unet_512
        train_datagen = segmentation_generator(train_img, mask_files=train_gt, img_preproc_func=skin_preprocess,
                                     mask_preproc_func=mask_preprocess, batch_size=BATCH_SIZE, augmentation=True,shuff=True)
        valid_datagen = segmentation_generator(valid_img, mask_files=valid_gt, img_preproc_func=skin_preprocess,
                                     mask_preproc_func=mask_preprocess, batch_size=BATCH_SIZE)
        test_datagen = segmentation_generator(test_img, mask_files=test_gt, img_preproc_func=skin_preprocess,
                                              mask_preproc_func=mask_preprocess, batch_size=TEST_BATCH_SIZE)
        train_steps = ceil(len(train_img) / BATCH_SIZE)
        valid_steps = ceil(len(valid_img) / BATCH_SIZE)
        model = get_unet_512()

    elif ARCHITECTURE == 'patch':
        TEST_BATCH_SIZE = 1
        n_samples = 512
        from patch_net import get_patch_net2
        train_datagen = patch_generator(train_img, mask_files=train_gt, img_preproc_func=skin_preprocess,
                                        mask_preproc_func=mask_preprocess, batch_size=BATCH_SIZE, augmentation=True,n_samples=n_samples)
        valid_datagen = patch_generator(valid_img, mask_files=valid_gt, img_preproc_func=skin_preprocess,
                                        mask_preproc_func=mask_preprocess, batch_size=BATCH_SIZE,n_samples=n_samples)
        test_datagen = evaluate_patch_generator(test_img, mask_files=test_gt, img_preproc_func=skin_preprocess,
                                                mask_preproc_func=mask_preprocess)
        train_steps = ceil(len(train_img)*n_samples/ BATCH_SIZE)
        valid_steps = ceil(len(valid_img)*n_samples / BATCH_SIZE)
        model = get_patch_net2()
    else:

        print("Invalid archtecture: ",ARCHITECTURE)


    model.compile(optimizer=RMSprop(lr=LR, decay=DECAY), loss=dice_loss, metrics=[iou, precision, recall, 'accuracy'])


    model_name = ARCHITECTURE + "_" + SOURCE_DATASET + '_LR' + str(LR) + '_DC' + str(DECAY)

    if WEIGHTS != 'none':
        model_name += '_('+WEIGHTS+')'
        model.load_weights(os.path.join(HDF5_DIR, WEIGHTS+".hdf5"))

    run = get_run()
    model_name_run = model_name + "_" + str(run)

    print("Training Model:", model_name_run)

    model.fit_generator(
            train_datagen,
            steps_per_epoch=train_steps,
            epochs=EPOCHS,
            validation_data=valid_datagen,
            validation_steps=valid_steps,
            callbacks=get_callbacks(model_name_run, log_dir="log", patience=PATIENCE, hdf5_dir=HDF5_DIR),
            workers=8 )

    if ARCHITECTURE == 'unet':

        print("Evaluating:", model_name_run)

        results = model.evaluate_generator(test_datagen, steps=ceil(len(test_img)/TEST_BATCH_SIZE) , workers=8)

        res_df = pd.DataFrame({'source': [SOURCE_DATASET],
                               'model': [model_name_run],
                               'architecure': [ARCHITECTURE],
                               'lr': [LR],
                               'decay': [DECAY],
                               'weights': [WEIGHTS]
                               }
                              )

        for metric, val in zip(model.metrics_names, results):
            res_df[metric] = round(val,4)
            print(metric+": "+"%.4f" % val)

        if os.path.exists("results/training_results.csv"):
            res_df= pd.read_csv("results/training_results_results.csv").append(res_df)

        res_df.to_csv("results/training_results_results.csv", index=False)




# Main Function
def Run():

    global SOURCE_DATASET, BATCH_SIZE, LR, DECAY, IMAGE_DIR, HDF5_DIR,  \
           TEST_PROPORTION, VALID_PROPORTION,PATIENCE, EPOCHS, ARCHITECTURE, WEIGHTS

    parser = argparse.ArgumentParser(description='Train model on dataset (no pseudo labels).')
    parser.add_argument("architecture", help="Architecture of the model.", type=str)
    parser.add_argument("source_dataset", help="Source Dataset", type=str, default=SOURCE_DATASET)
    parser.add_argument("--epochs", help="Number of epochs.", type=float, default=EPOCHS, required=False)
    parser.add_argument("--weights", help="Model weights to load before training.", type=str, default=WEIGHTS, required=False)
    parser.add_argument("--patience", help="Patience.", type=float, default=PATIENCE, required=False)
    parser.add_argument("--batch_size", help="Batch size.", type=int, default=BATCH_SIZE, required=False)
    parser.add_argument("--lr", help="Learning rate.", type=float, default=LR, required=False)
    parser.add_argument("--decay", help="Learning rate decay.", type=float, default=DECAY, required=False)
    parser.add_argument("--image_dir", help="Image directory", type=str, default=IMAGE_DIR, required=False)
    parser.add_argument("--hdf5_dir", help="HDF5 Directort", type=str, default=HDF5_DIR, required=False)
    parser.add_argument("--test_proportion", help="Test Proportion.", type=float, default=TEST_PROPORTION, required=False)
    parser.add_argument("--valid_proportion", help="Valid Proportion.", type=float, default=VALID_PROPORTION, required=False)

    args = parser.parse_args()

    ARCHITECTURE = args.architecture
    SOURCE_DATASET = args.source_dataset
    WEIGHTS = args.weights
    BATCH_SIZE = args.batch_size
    EPOCHS = args.epochs
    PATIENCE = args.patience
    LR = args.lr
    DECAY = args.decay
    IMAGE_DIR = args.image_dir
    HDF5_DIR = args.hdf5_dir
    TEST_PROPORTION = args.test_proportion
    VALID_PROPORTION = args.valid_proportion

    train()


if __name__ == '__main__':
  Run()
