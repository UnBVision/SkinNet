import os
os.environ["KERAS_BACKEND"]="tensorflow"
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import numpy as np # linear algebra
from keras.optimizers import RMSprop
import cv2
from unet_512_768 import get_unet_512
from patch_net import get_patch_net2
from dnn_utils import get_images, skin_preprocess, mask_preprocess, dice_loss, iou
from dnn_utils import eval_jaccard, eval_acc, evaluation_generator, split, eval_precision, eval_recall, evaluate_patch_generator
import argparse
from tqdm import tqdm


##################################

BATCH_SIZE = 4
LR=0.0001
DECAY=0.002

SIZE=768

IMAGE_DIR = "/d02/data/skin/images"
PSEUDO_DIR = "/d02/data/skin/pseudo"
HDF5_DIR = "weights"
DATASET = "compaq" #compaq, sfa, pratheepan, vpu
TEST_PROPORTION = 1
TEST_BATCH_SIZE = 15
MODEL = ""
ARCHITECTURE = 'unet'
##################################


def pseudo_labels():

    global TEST_PROPORTION

    imgs, gts = get_images(DATASET, IMAGE_DIR)

    if TEST_PROPORTION == 1:
        VALID_PROPORTION = 0
    elif TEST_PROPORTION < .5:
        VALID_PROPORTION = TEST_PROPORTION
    else:
        VALID_PROPORTION = (1 - TEST_PROPORTION)/2


    _, _, _, _, test_img, test_gt = split(imgs, gts, test_p = TEST_PROPORTION, valid_p=VALID_PROPORTION)

    if ARCHITECTURE == 'unet':
        model = get_unet_512()
        test_datagen = evaluation_generator(test_img, mask_files=test_gt, img_preproc_func=skin_preprocess,
        mask_preproc_func=mask_preprocess)

    elif ARCHITECTURE == 'patch':
        model = get_patch_net2()
        patch_datagen = evaluate_patch_generator(test_img, mask_files=test_gt, img_preproc_func=skin_preprocess,
                                                 mask_preproc_func=mask_preprocess)

    else:
        print("Invalid archtecture: ", ARCHITECTURE)
        exit(-1)

    model.compile(optimizer=RMSprop(lr=LR, decay=DECAY), loss=dice_loss, metrics=[iou, 'accuracy'])


    ################################################
    print ("\n\nGenerating pseudo labels for dataset", DATASET, "from model", MODEL)
    ################################################

    model.load_weights(os.path.join(HDF5_DIR, MODEL + ".hdf5"))

    TEST_STEPS = len(test_img)

    #TEST_STEPS = 10
    for i in tqdm(range(TEST_STEPS)):

        img = cv2.imread(test_img[i])

        if ARCHITECTURE == 'unet' :
            # UNet Prediction
            _img, mask = next(test_datagen)

            unet_prediction = model.predict(_img)[0]

            # fixing size
            (h, w) = mask.shape[:2]
            prediction = unet_prediction[(SIZE - h) // 2: (SIZE - h) // 2 + h, (SIZE - w) // 2: (SIZE - w) // 2 + w]

        else:
            patch, mask = next(patch_datagen)

            prediction = model.predict(patch)

        prediction = np.round(prediction.reshape(prediction.shape[0], prediction.shape[1])) * 255

        if prediction.shape != (img.shape[0], img.shape[1]):
            prediction = cv2.resize(prediction, (img.shape[1], img.shape[0]))
            prediction = cv2.cvtColor(prediction, cv2.COLOR_GRAY2BGR)

        path = os.path.join(os.path.join(PSEUDO_DIR, MODEL), test_img[i][(1+len(IMAGE_DIR)):])
        if not os.path.exists(os.path.dirname(path)):
            os.makedirs(os.path.dirname(path))

        #print(path)


        cv2.imwrite(path, prediction)


# Main Function
def Run():

    global DATASET, MODEL, IMAGE_DIR, HDF5_DIR,  PSEUDO_DIR, ARCHITECTURE

    parser = argparse.ArgumentParser(description='Train on source dataset.')
    parser.add_argument("architecture", help="architecture ", type=str, default=ARCHITECTURE)
    parser.add_argument("model", help="Model saved weights", type=str, default=DATASET)
    parser.add_argument("dataset", help="Dataset to evaluate", type=str, default=DATASET)
    parser.add_argument("--image_dir", help="Image directory", type=str, default=IMAGE_DIR)
    parser.add_argument("--pseudo_dir", help="Pseudo Labels directory", type=str, default=PSEUDO_DIR)
    parser.add_argument("--hdf5_dir", help="HDF5 Directort", type=str, default=HDF5_DIR)

    args = parser.parse_args()

    ARCHITECTURE = args.architecture
    MODEL = args.model
    DATASET = args.dataset
    IMAGE_DIR = args.image_dir
    HDF5_DIR = args.hdf5_dir
    PSEUDO_DIR = args.pseudo_dir

    pseudo_labels()


if __name__ == '__main__':
  Run()





