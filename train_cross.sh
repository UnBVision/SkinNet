#Source COMPAQ ########################################################################################################################################

############
#Pratheepan
############

#target Only
#python train_cross.py unet pratheepan --epochs=100 --patience=20 --lr=0.0005 --decay=0.01 --train_proportion=0.05

#python train_cross.py unet pratheepan --epochs=100 --patience=20 --lr=0.0005 --decay=0.01 --train_proportion=.1

#python train_cross.py unet pratheepan --epochs=100 --patience=20 --lr=0.0005 --decay=0.01 --train_proportion=.5

#python train_cross.py unet pratheepan --epochs=100 --patience=20 --lr=0.0005 --decay=0.01 --train_proportion=1


#Fine Only

#python train_cross.py unet pratheepan --weights=unet_compaq_LR0.0001_DC0.002_11 --epochs=100 --patience=20 --lr=0.00001 --decay=0.002 --train_proportion=.05

#python train_cross.py unet pratheepan --weights=unet_compaq_LR0.0001_DC0.002_11 --epochs=100 --patience=20 --lr=0.00001 --decay=0.002 --train_proportion=.1

#python train_cross.py unet pratheepan --weights=unet_compaq_LR0.0001_DC0.002_11 --epochs=100 --patience=20 --lr=0.00001 --decay=0.002 --train_proportion=.50

#python train_cross.py unet pratheepan --weights=unet_compaq_LR0.0001_DC0.002_11 --epochs=100 --patience=20 --lr=0.00001 --decay=0.002 --train_proportion=1



#Pseudo Only
#python train_cross.py unet pratheepan --pseudo_model=unet_compaq_LR0.0001_DC0.002_11 --epochs=100 --patience=20 --lr=0.0005 --decay=0.01 --train_proportion=0

#python train_cross.py unet pratheepan --pseudo_model=unet_compaq_LR0.0001_DC0.002_11 --epochs=100 --patience=20 --lr=0.0005 --decay=0.01 --train_proportion=0.05

#python train_cross.py unet pratheepan --pseudo_model=unet_compaq_LR0.0001_DC0.002_11 --epochs=100 --patience=20 --lr=0.0005 --decay=0.01 --train_proportion=.1

#python train_cross.py unet pratheepan --pseudo_model=unet_compaq_LR0.0001_DC0.002_11 --epochs=100 --patience=20 --lr=0.0005 --decay=0.01 --train_proportion=.5


#Combined
#python pseudo_labels.py unet  unet_pratheepan_P_LR0.0005_DC0.01_265 pratheepan
#python train_cross.py unet pratheepan --pseudo_model=unet_pratheepan_P_LR0.0005_DC0.01_265 --weights=unet_pratheepan_P_LR0.0005_DC0.01_265 --epochs=100 --patience=30 --lr=0.0001 --#decay=0.002 --train_proportion=.0

#python pseudo_labels.py unet  unet_pratheepan_P_LR0.0005_DC0.01_266 pratheepan
#python train_cross.py unet pratheepan --pseudo_model=unet_pratheepan_P_LR0.0005_DC0.01_266 --weights=unet_pratheepan_P_LR0.0005_DC0.01_266 --epochs=100 --patience=30 --lr=0.0001 --#decay=0.002 --train_proportion=0.05

#python pseudo_labels.py unet  unet_pratheepan_P_LR0.0005_DC0.01_246 pratheepan
#python train_cross.py unet pratheepan --pseudo_model=unet_pratheepan_P_LR0.0005_DC0.01_246 --weights=unet_pratheepan_P_LR0.0005_DC0.01_246 --epochs=100 --patience=30 --lr=0.0001 --#decay=0.002 --train_proportion=0.1

#python pseudo_labels.py unet  unet_pratheepan_P_LR0.0005_DC0.01_245 pratheepan
#python train_cross.py unet pratheepan --pseudo_model=unet_pratheepan_P_LR0.0005_DC0.01_245 --weights=unet_pratheepan_P_LR0.0005_DC0.01_245 --epochs=100 --patience=30 --lr=0.0001 --#decay=0.002 --train_proportion=.5

############
#SFA
############

#target Only

#python train_cross.py unet sfa --epochs=100 --patience=20 --lr=0.0002 --decay=0.002 --train_proportion=0.05
#python train_cross.py unet sfa --epochs=100 --patience=20 --lr=0.0002 --decay=0.002 --train_proportion=0.1
#python train_cross.py unet sfa --epochs=100 --patience=20 --lr=0.0002 --decay=0.002 --train_proportion=0.5
#python train_cross.py unet sfa --epochs=100 --patience=20 --lr=0.0002 --decay=0.002 --train_proportion=1

#fine tuning Only

#python train_cross.py unet sfa --weights=unet_compaq_LR0.0001_DC0.002_11 --epochs=100 --patience=30 --lr=0.00008 --decay=0.001 --train_proportion=0.05
#python train_cross.py unet sfa --weights=unet_compaq_LR0.0001_DC0.002_11 --epochs=100 --patience=30 --lr=0.00008 --decay=0.001 --train_proportion=0.1
#python train_cross.py unet sfa --weights=unet_compaq_LR0.0001_DC0.002_11 --epochs=100 --patience=30 --lr=0.00008 --decay=0.001 --train_proportion=0.5
#python train_cross.py unet sfa --weights=unet_compaq_LR0.0001_DC0.002_11 --epochs=100 --patience=30 --lr=0.00008 --decay=0.001 --train_proportion=1


#Pseudo Only
#python train_cross.py unet sfa --pseudo_model=unet_compaq_LR0.0001_DC0.002_11 --epochs=100 --patience=20 --lr=0.0005 --decay=0.002 --train_proportion=0

#python train_cross.py unet sfa --pseudo_model=unet_compaq_LR0.0001_DC0.002_11 --epochs=100 --patience=20 --lr=0.0005 --decay=0.002 --train_proportion=0.05

#python train_cross.py unet sfa --pseudo_model=unet_compaq_LR0.0001_DC0.002_11 --epochs=100 --patience=20 --lr=0.00005 --decay=0.002 --train_proportion=.1

#python train_cross.py unet sfa --pseudo_model=unet_compaq_LR0.0001_DC0.002_11 --epochs=100 --patience=20 --lr=0.00005 --decay=0.002 --train_proportion=.5

#combined
#python pseudo_labels.py unet  unet_sfa_P_LR0.0005_DC0.002_296 sfa
#python train_cross.py unet sfa --pseudo_model=unet_sfa_P_LR0.0005_DC0.002_296 --weights=unet_sfa_P_LR0.0005_DC0.002_296 --epochs=100 --patience=30 --lr=0.00008 --decay=0.001 --train_proportion=0

#python pseudo_labels.py unet  unet_sfa_P_LR0.0005_DC0.002_297 sfa
#python train_cross.py unet sfa --pseudo_model=unet_sfa_P_LR0.0005_DC0.002_297 --weights=unet_sfa_P_LR0.0005_DC0.002_297 --epochs=100 --patience=30 --lr=0.00008 --decay=0.001 --train_proportion=0.05

#python pseudo_labels.py unet  unet_sfa_P_LR5e-05_DC0.002_298 sfa
#python train_cross.py unet sfa --pseudo_model=unet_sfa_P_LR5e-05_DC0.002_298 --weights=unet_sfa_P_LR5e-05_DC0.002_298 --epochs=100 --patience=30 --lr=0.00008 --decay=0.001 --train_proportion=0.1

#python pseudo_labels.py unet  unet_sfa_P_LR5e-05_DC0.002_299 sfa
#python train_cross.py unet sfa --pseudo_model=unet_sfa_P_LR5e-05_DC0.002_299 --weights=unet_sfa_P_LR5e-05_DC0.002_299 --epochs=100 --patience=30 --lr=0.00008 --decay=0.001 --train_proportion=0.5


############
#VPU
############

#target Only

#python train_cross.py unet vpu --epochs=100 --patience=20 --lr=0.0005 --decay=0.01 --train_proportion=0.05
#python train_cross.py unet vpu --epochs=100 --patience=20 --lr=0.0005 --decay=0.01 --train_proportion=0.1
#python train_cross.py unet vpu --epochs=100 --patience=20 --lr=0.0005 --decay=0.01 --train_proportion=0.5
#python train_cross.py unet vpu --epochs=100 --patience=20 --lr=0.0005 --decay=0.01 --train_proportion=1


#fine only
#python train_cross.py unet vpu --weights=unet_compaq_LR0.0001_DC0.002_11 --epochs=100 --patience=20 --lr=0.00005 --decay=0.002 --train_proportion=.05
#python train_cross.py unet vpu --weights=unet_compaq_LR0.0001_DC0.002_11 --epochs=100 --patience=20 --lr=0.00005 --decay=0.002 --train_proportion=.1
#python train_cross.py unet vpu --weights=unet_compaq_LR0.0001_DC0.002_11 --epochs=100 --patience=20 --lr=0.00005 --decay=0.002 --train_proportion=.50
#python train_cross.py unet vpu --weights=unet_compaq_LR0.0001_DC0.002_11 --epochs=100 --patience=20 --lr=0.00005 --decay=0.002 --train_proportion=1


#Pseudo Only
#python train_cross.py unet vpu --pseudo_model=unet_compaq_LR0.0001_DC0.002_11 --epochs=100 --patience=20 --lr=0.0005 --decay=0.002 --train_proportion=0
#python train_cross.py unet vpu --pseudo_model=unet_compaq_LR0.0001_DC0.002_11 --epochs=100 --patience=20 --lr=0.0005 --decay=0.002 --train_proportion=0.05
#python train_cross.py unet vpu --pseudo_model=unet_compaq_LR0.0001_DC0.002_11 --epochs=100 --patience=20 --lr=0.00005 --decay=0.002 --train_proportion=.1
#python train_cross.py unet vpu --pseudo_model=unet_compaq_LR0.0001_DC0.002_11 --epochs=100 --patience=20 --lr=0.00005 --decay=0.002 --train_proportion=.5

#combined
#python pseudo_labels.py unet  unet_vpu_P_LR0.0005_DC0.002_316 vpu
#python train_cross.py unet vpu --pseudo_model=unet_vpu_P_LR0.0005_DC0.002_316 --weights=unet_vpu_P_LR0.0005_DC0.002_316 --epochs=100 --patience=30 --lr=0.00005 --decay=0.002 --train_proportion=0

#python pseudo_labels.py unet  unet_vpu_P_LR0.0005_DC0.002_317 vpu
#python train_cross.py unet vpu --pseudo_model=unet_vpu_P_LR0.0005_DC0.002_317 --weights=unet_vpu_P_LR0.0005_DC0.002_317 --epochs=100 --patience=30 --lr=0.00005 --decay=0.002 --train_proportion=0.05

#python pseudo_labels.py unet  unet_vpu_P_LR5e-05_DC0.002_318 vpu
#python train_cross.py unet vpu --pseudo_model=unet_vpu_P_LR5e-05_DC0.002_318 --weights=unet_vpu_P_LR5e-05_DC0.002_318 --epochs=100 --patience=30 --lr=0.00005 --decay=0.002 --train_proportion=0.1

#python pseudo_labels.py unet  unet_vpu_P_LR5e-05_DC0.002_319 vpu
#python train_cross.py unet vpu --pseudo_model=unet_vpu_P_LR5e-05_DC0.002_319 --weights=unet_vpu_P_LR5e-05_DC0.002_319 --epochs=100 --patience=30 --lr=0.00005 --decay=0.002 --train_proportion=0.5

#Source Pratheepan ########################################################################################################################################

############
#SFA
############

#target Only

#Já feito

#fine tuning Only

#python train_cross.py unet sfa --weights=unet_pratheepan_LR0.0001_DC0.002_12 --epochs=100 --patience=30 --lr=0.00008 --decay=0.001 --train_proportion=0.05
#python train_cross.py unet sfa --weights=unet_pratheepan_LR0.0001_DC0.002_12 --epochs=100 --patience=30 --lr=0.00008 --decay=0.001 --train_proportion=0.1
#python train_cross.py unet sfa --weights=unet_pratheepan_LR0.0001_DC0.002_12 --epochs=100 --patience=30 --lr=0.00008 --decay=0.001 --train_proportion=0.5
#python train_cross.py unet sfa --weights=unet_pratheepan_LR0.0001_DC0.002_12 --epochs=100 --patience=30 --lr=0.00008 --decay=0.001 --train_proportion=1


#Pseudo Only

#python train_cross.py unet sfa --pseudo_model=unet_pratheepan_LR0.0001_DC0.002_12 --epochs=100 --patience=20 --lr=0.0005 --decay=0.002 --train_proportion=0
#python train_cross.py unet sfa --pseudo_model=unet_pratheepan_LR0.0001_DC0.002_12 --epochs=100 --patience=20 --lr=0.0005 --decay=0.002 --train_proportion=0.05
#python train_cross.py unet sfa --pseudo_model=unet_pratheepan_LR0.0001_DC0.002_12 --epochs=100 --patience=20 --lr=0.0005 --decay=0.002 --train_proportion=.1
#python train_cross.py unet sfa --pseudo_model=unet_pratheepan_LR0.0001_DC0.002_12 --epochs=100 --patience=20 --lr=0.0005 --decay=0.002 --train_proportion=.5

#combined
#python pseudo_labels.py unet  unet_sfa_P_LR0.0005_DC0.002_329 sfa
#python train_cross.py unet sfa --pseudo_model=unet_sfa_P_LR0.0005_DC0.002_329 --weights=unet_sfa_P_LR0.0005_DC0.002_329 --epochs=100 --patience=30 --lr=0.00008 --decay=0.001 --train_proportion=0

#python pseudo_labels.py unet  unet_sfa_P_LR0.0005_DC0.002_330 sfa
#python train_cross.py unet sfa --pseudo_model=unet_sfa_P_LR0.0005_DC0.002_330 --weights=unet_sfa_P_LR0.0005_DC0.002_330 --epochs=100 --patience=30 --lr=0.00008 --decay=0.001 --train_proportion=0.05

#python pseudo_labels.py unet  unet_sfa_P_LR0.0005_DC0.002_331 sfa
#python train_cross.py unet sfa --pseudo_model=unet_sfa_P_LR0.0005_DC0.002_331 --weights=unet_sfa_P_LR0.0005_DC0.002_331 --epochs=100 --patience=30 --lr=0.00008 --decay=0.001 --train_proportion=0.1

#python pseudo_labels.py unet  unet_sfa_P_LR0.0005_DC0.002_332 sfa
#python train_cross.py unet sfa --pseudo_model=unet_sfa_P_LR0.0005_DC0.002_332 --weights=unet_sfa_P_LR0.0005_DC0.002_332 --epochs=100 --patience=30 --lr=0.00008 --decay=0.001 --train_proportion=0.5

############
#Compaq
############

#target Only

#Já feito

#fine tuning Only

python train_cross.py unet compaq --weights=unet_pratheepan_LR0.0001_DC0.002_12 --epochs=100 --patience=30 --lr=0.00008 --decay=0.001 --train_proportion=0.05
python train_cross.py unet compaq --weights=unet_pratheepan_LR0.0001_DC0.002_12 --epochs=100 --patience=30 --lr=0.00008 --decay=0.001 --train_proportion=0.1
python train_cross.py unet compaq --weights=unet_pratheepan_LR0.0001_DC0.002_12 --epochs=100 --patience=30 --lr=0.00008 --decay=0.001 --train_proportion=0.5
python train_cross.py unet compaq --weights=unet_pratheepan_LR0.0001_DC0.002_12 --epochs=100 --patience=30 --lr=0.00008 --decay=0.001 --train_proportion=1


#Pseudo Only

python train_cross.py unet compaq --pseudo_model=unet_pratheepan_LR0.0001_DC0.002_12 --epochs=100 --patience=20 --lr=0.0005 --decay=0.002 --train_proportion=0
python train_cross.py unet compaq --pseudo_model=unet_pratheepan_LR0.0001_DC0.002_12 --epochs=100 --patience=20 --lr=0.0005 --decay=0.002 --train_proportion=0.05
python train_cross.py unet compaq --pseudo_model=unet_pratheepan_LR0.0001_DC0.002_12 --epochs=100 --patience=20 --lr=0.0005 --decay=0.002 --train_proportion=.1
python train_cross.py unet compaq --pseudo_model=unet_pratheepan_LR0.0001_DC0.002_12 --epochs=100 --patience=20 --lr=0.0005 --decay=0.002 --train_proportion=.5

#combined
#python pseudo_labels.py unet  unet_sfa_P_LR0.0005_DC0.002_329 sfa
#python train_cross.py unet sfa --pseudo_model=unet_sfa_P_LR0.0005_DC0.002_329 --weights=unet_sfa_P_LR0.0005_DC0.002_329 --epochs=100 --patience=30 --lr=0.00008 --decay=0.001 --train_proportion=0

#python pseudo_labels.py unet  unet_sfa_P_LR0.0005_DC0.002_330 sfa
#python train_cross.py unet sfa --pseudo_model=unet_sfa_P_LR0.0005_DC0.002_330 --weights=unet_sfa_P_LR0.0005_DC0.002_330 --epochs=100 --patience=30 --lr=0.00008 --decay=0.001 --train_proportion=0.05

#python pseudo_labels.py unet  unet_sfa_P_LR0.0005_DC0.002_331 sfa
#python train_cross.py unet sfa --pseudo_model=unet_sfa_P_LR0.0005_DC0.002_331 --weights=unet_sfa_P_LR0.0005_DC0.002_331 --epochs=100 --patience=30 --lr=0.00008 --decay=0.001 --train_proportion=0.1

#python pseudo_labels.py unet  unet_sfa_P_LR0.0005_DC0.002_332 sfa
#python train_cross.py unet sfa --pseudo_model=unet_sfa_P_LR0.0005_DC0.002_332 --weights=unet_sfa_P_LR0.0005_DC0.002_332 --epochs=100 --patience=30 --lr=0.00008 --decay=0.001 --train_proportion=0.5


############
#VPU
############

#target Only

#já feito


#fine only
#python train_cross.py unet vpu --weights=unet_pratheepan_LR0.0001_DC0.002_12 --epochs=150 --patience=20 --lr=0.0003 --decay=0.01 --train_proportion=.05
#python train_cross.py unet vpu --weights=unet_pratheepan_LR0.0001_DC0.002_12 --epochs=150 --patience=20 --lr=0.0003 --decay=0.01 --train_proportion=.1
#python train_cross.py unet vpu --weights=unet_pratheepan_LR0.0001_DC0.002_12 --epochs=150 --patience=20 --lr=0.0003 --decay=0.01 --train_proportion=.50
#python train_cross.py unet vpu --weights=unet_pratheepan_LR0.0001_DC0.002_12 --epochs=150 --patience=20 --lr=0.0003 --decay=0.01 --train_proportion=1


#Pseudo Only

#python pseudo_labels.py unet  unet_pratheepan_LR0.0001_DC0.002_12 vpu

#python train_cross.py unet vpu --pseudo_model=unet_pratheepan_LR0.0001_DC0.002_12 --epochs=100 --patience=20 --lr=0.0005 --decay=0.002 --train_proportion=0
#python train_cross.py unet vpu --pseudo_model=unet_pratheepan_LR0.0001_DC0.002_12 --epochs=100 --patience=20 --lr=0.0005 --decay=0.002 --train_proportion=0.05
#python train_cross.py unet vpu --pseudo_model=unet_pratheepan_LR0.0001_DC0.002_12 --epochs=100 --patience=20 --lr=0.0005 --decay=0.002 --train_proportion=.1
#python train_cross.py unet vpu --pseudo_model=unet_pratheepan_LR0.0001_DC0.002_12 --epochs=100 --patience=20 --lr=0.0005 --decay=0.002 --train_proportion=.5

#combined
#python pseudo_labels.py unet  unet_vpu_P_LR0.0005_DC0.002_349 vpu
#python train_cross.py unet vpu --pseudo_model=unet_vpu_P_LR0.0005_DC0.002_349 --weights=unet_vpu_P_LR0.0005_DC0.002_349 --epochs=100 --patience=30 --lr=0.00005 --decay=0.002 --train_proportion=0

#python pseudo_labels.py unet  unet_vpu_P_LR0.0005_DC0.002_350 vpu
#python train_cross.py unet vpu --pseudo_model=unet_vpu_P_LR0.0005_DC0.002_350 --weights=unet_vpu_P_LR0.0005_DC0.002_350 --epochs=100 --patience=30 --lr=0.00005 --decay=0.002 --train_proportion=0.05

#python pseudo_labels.py unet  unet_vpu_P_LR0.0005_DC0.002_351 vpu
#python train_cross.py unet vpu --pseudo_model=unet_vpu_P_LR0.0005_DC0.002_351 --weights=unet_vpu_P_LR0.0005_DC0.002_351 --epochs=100 --patience=30 --lr=0.00005 --decay=0.002 --train_proportion=0.1

#python pseudo_labels.py unet  unet_vpu_P_LR0.0005_DC0.002_352 vpu
#python train_cross.py unet vpu --pseudo_model=unet_vpu_P_LR0.0005_DC0.002_352 --weights=unet_vpu_P_LR0.0005_DC0.002_352 --epochs=100 --patience=30 --lr=0.00005 --decay=0.002 --train_proportion=0.5

